<!DOCTYPE html>
<html>

<head>
    <link rel="stylesheet" type="text/css" href="estilos.css">
    <meta charset="utf-8">
</head>

<body>
    <div>
    <h1>Pedidos</h1>
    <h2>Menú</h2>
    <?php
     include("util.php");

    $campos = array("ID","Fecha","idCliente","Notas"); // los campos que contiene tanto el formulario como la tabla
    global $f_articulos;
    $fichero = $f_articulos;

    // interceptamos la opción elegida y mostramos un menú u otro, según convenga
    if ($op=="e") { // resultado de un formulario
        $array = validarDatos($campos);
        procesaResultado($array);
    }
    else
        mostrarMenu(); // mostramos el menú principal
   
    ?>
    <span class="limpia"></span>
    <footer><p>Evaluable 1 DWES | Ángel de Fez | 13-11-2019</p></footer>
    </div>
    <?php
    // muestra el menú de cada página. al tratarse de objetos, cada uno tiene su propio menú
    function mostrarMenu() {
        global $op, $url_actual, $id, $usuario, $campos;
        $objetos = PedidoReadAll();
        switch ($op) {
            case "c":
                $nuevaID = Pedido::getUltimaID()+1;
                mostrarFormulario($nuevaID, $op);
                crearEnlace($url_actual."?usuario=$usuario","Volver","derecha");
                break;
            case "r":
                crearEnlace($url_actual."?op=c&usuario=$usuario","Crear nuevo","derecha");
                $o[] = PedidoRead($id);
                muestraTabla($o, $campos, false);
                muestraLineas($o);
                crearEnlace($url_actual."?usuario=$usuario","Volver","derecha");
                break;
            case "u":
                mostrarFormulario($id, $op);
                crearEnlace($url_actual."?usuario=$usuario","Volver","derecha");
                break;
            case "d":
                PedidoDelete($id);
                $objetos = PedidoReadAll(); // actualizamos los datos
                crearEnlace($url_actual."?op=c&usuario=$usuario","Crear nuevo","derecha");
                muestraTabla($objetos, $campos, true);
                break;
            case "e": // resultado de un formulario
                    $tabla = array();
                    $tabla["ID"] = (isset($_REQUEST["ID"])) ? $_REQUEST["ID"] : null;
                    $tabla["Fecha"] = (isset($_REQUEST["Fecha"])) ? $_REQUEST["Fecha"] : null;
                    $tabla["idCliente"] = (isset($_REQUEST["idCliente"])) ? $_REQUEST["idCliente"] : null;
                    $tabla["Notas"] = (isset($_REQUEST["Notas"])) ? $_REQUEST["Notas"] : null;
                    $array = validarDatos($tabla, $campos);
                    procesaResultado($tabla, $array);
                    break;
            default:
                crearEnlace($url_actual."?op=c&usuario=$usuario","Crear nuevo","derecha");
                muestraTabla($objetos, $campos, true);
        }
        crearEnlace("index.php?usuario=$usuario","Inicio","derecha");
    }

    // muestra el formulario, cada tabla tiene el suyo propio
        function mostrarFormulario($id, $op) {
            $objeto = PedidoRead($id);
            $fecha = ($objeto) ? $objeto->getFecha() : null; // si existe el objeto, llenamos los campos
            $idCliente = ($objeto) ? $objeto->getIDCliente() : null;
            $notas = ($objeto) ? $objeto->getNotas() : null;
            
            global $usuario;
            $titulo = ($op == 'c') ? "Crear Nuevo":"Modificar";
        
            echo '<form name="formulario" method="post" action="pedidos.php?op=e&id='.$id.'&usuario='.$usuario.'">';
            echo '<fieldset>';
            echo '<legend>'.$titulo.' Artículo</legend>';
            echo '<p>ID: <input type="text" name="ID" size="5" required readonly value='.$id.'></p>'; // sólo lectura, no se puede modificar
            echo '<p>Fecha: <input type="text" name="Fecha" size="10" required value="'.$fecha.'"></p>';
            echo '<p>ID cliente: <input type="text" name="idCliente" size="6" required value="'.$idCliente.'"></p>';
            echo 'Notas: <br><textarea name="Notas" rows="5" cols="80">'.$notas.'</textarea><br>'; // no sé por qué pero se bloquea en Firefox
            echo '<input type="submit" value="Enviar">';
            echo '<input type="reset" value="Reiniciar">';
            echo '<input name="o" type="hidden" value="'.$op.'">';
            echo '</fieldset>';
            echo '</form>';
        }

        function crearNuevo($tabla) {
            PedidoCreate(new Pedido($tabla["ID"],$tabla["Fecha"],$tabla["idCliente"],$tabla["Notas"]));
        }
    
        function actualizarTabla($tabla) {
            PedidoUpdate(new Pedido($tabla["ID"],$tabla["Fecha"],$tabla["idCliente"],$tabla["Notas"]));
        }

        function muestraLineas($o) {
            echo "<h3>Detalle del pedido</h3>";
            $lineas = LineaReadAll(); // cargamos todos los datos de líneas que tenemos            

            $ped = $o[0]; // el objeto de Pedido que estamos viendo
            $pedidos = []; // aquí almacenaremos todas las líneas que sean del pedido
            foreach ($lineas as $l) {
                if ($l->getIdPedido() == $ped->getId() && $l->getIdCliente() == $ped->getIdCliente())
                    $pedidos[] = $l; // añadimos a la lista el pedido indicado si coinciden idPedido e idCliente
            }

            $detalles = []; // nombre, precio, unidades y total del artículo
            $total = 0; // sumatorio del pedido
            foreach($pedidos as $p) {
                $n = $p->getCantidad();
                $a = ArticuloRead($p->getIdProducto());
                $total += $n*$a->getPrecio();
                
                $detalles [] = array("Articulo" => $a->getNombre(), "Cantidad" => $n,
                "Precio" => $a->getPrecio()."€", "Total" => $n*$a->getPrecio()."€");
            }
            
            echo "<table>";
            arrayAtabla($detalles); // mostramos los detalles del pedido en una tabla
            echo "<tr><th class='derecha' colspan='3'>TOTAL PEDIDO</th><th>$total"."€</th></tr>";
            echo "</table>";
        }
    ?>

</body>
</html>