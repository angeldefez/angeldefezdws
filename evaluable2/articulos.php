<!DOCTYPE html>
<html>

<head>
    <link rel="stylesheet" type="text/css" href="estilos.css">
    <meta charset="utf-8">
</head>

<body>
    <div>
    <h1>Artículos</h1>
    <h2>Menú</h2>

    <?php
    include("util.php");

    $campos = array("ID","Nombre","Precio","Descripcion");
    global $f_articulos;
    $fichero = $f_articulos;

    // interceptamos la opción elegida y mostramos un menú u otro, según convenga
    if ($op=="e") { // resultado de un formulario
        $array = validarDatos($campos);
        procesaResultado($array);
    }
    else
        mostrarMenu(); // mostramos el menú principal

    ?>
    <span class="limpia"></span>
    <footer><p>Evaluable 1 DWES | Ángel de Fez | 13-11-2019</p></footer>
    </div>
    <?php
    // muestra el menú de cada página. al tratarse de objetos, cada uno tiene su propio menú
    function mostrarMenu() {
        global $op, $url_actual, $id, $usuario, $campos;
        $objetos = ArticuloReadAll();
        switch ($op) {
            case "c":
                $nuevaID = Articulo::getUltimaID()+1;
                mostrarFormulario($nuevaID, $op);
                crearEnlace($url_actual."?usuario=$usuario","Volver","derecha");
                break;
            case "r":
                crearEnlace($url_actual."?op=c&usuario=$usuario","Crear nuevo","derecha");
                $o[]= ArticuloRead($id);
                muestraTabla($o, $campos, false);
                crearEnlace($url_actual."?usuario=$usuario","Volver","derecha");
                break;
            case "u":
                mostrarFormulario($id, $op);
                crearEnlace($url_actual."?usuario=$usuario","Volver","derecha");
                break;
            case "d":
                ArticuloDelete($id);
                $objetos = ArticuloReadAll(); // actualizamos los datos
                crearEnlace($url_actual."?op=c&usuario=$usuario","Crear nuevo","derecha");
                muestraTabla($objetos, $campos, true);
                break;
            case "e": // resultado de un formulario
                    $tabla = array();
                    $tabla["ID"] = (isset($_REQUEST["ID"])) ? $_REQUEST["ID"] : null;
                    $tabla["Nombre"] = (isset($_REQUEST["Nombre"])) ? $_REQUEST["Nombre"] : null;
                    $tabla["Precio"] = (isset($_REQUEST["Precio"])) ? $_REQUEST["Precio"] : null;
                    $tabla["Descripcion"] = (isset($_REQUEST["Descripcion"])) ? $_REQUEST["Descripcion"] : null;
                    $array = validarDatos($tabla, $campos);
                    procesaResultado($tabla, $array);
                    break;
            default:
                crearEnlace($url_actual."?op=c&usuario=$usuario","Crear nuevo","derecha");
                muestraTabla($objetos, $campos, true);
        }
        crearEnlace("index.php?usuario=$usuario","Inicio","derecha");
    }

    // muestra el formulario, cada tabla tiene el suyo propio
    function mostrarFormulario($id, $op) {
        $objeto = ArticuloRead($id);
        $nombre = ($objeto) ? $objeto->getNombre() : null; // si existe el objeto, llenamos los campos
        $precio = ($objeto) ? $objeto->getPrecio() : null;
        $descripcion = ($objeto) ? $objeto->getDescripcion() : null;
        global $usuario;
        $titulo = ($op == 'c') ? "Crear Nuevo":"Modificar";
    
        echo '<form name="formulario" method="post" action="articulos.php?op=e&id='.$id.'&usuario='.$usuario.'">';
        echo '<fieldset>';
        echo '<legend>'.$titulo.' Artículo</legend>';
        echo '<p>ID: <input type="text" name="ID" size="5" required readonly value='.$id.'></p>'; // sólo lectura, no se puede modificar
        echo '<p>Nombre: <input type="text" name="Nombre" size="80" required value="'.$nombre.'"></p>';
        echo '<p>Precio: <input type="text" name="Precio" size="6" required value="'.$precio.'"></p>';
        echo 'Descripción: <p><textarea name="Descripcion" rows="5" cols="50">'.$descripcion.'</textarea></p>'; // no sé por qué pero se bloquea en Firefox
        echo '<input name="o" type="hidden" value="'.$op.'">';
        echo '<input type="submit" value="Enviar">';
        echo '<input type="reset" value="Reiniciar">';
        echo '</fieldset>';
        echo '</form>';
    }

    function crearNuevo($tabla) {
        ArticuloCreate(new Articulo($tabla["ID"],$tabla["Nombre"],$tabla["Precio"],$tabla["Descripcion"]));
    }

    function actualizarTabla($tabla) {
        ArticuloUpdate(new Articulo($tabla["ID"],$tabla["Nombre"],$tabla["Precio"],$tabla["Descripcion"]));
    }
    ?>
</body>
</html>