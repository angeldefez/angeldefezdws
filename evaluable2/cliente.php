<?php
class Cliente {
	// propiedades
    private $id;
    private $nombre;
    private $email;
    private $usuario;
    private $password;
    private $dni;
    private $foto;

    public static $cont = 0; // será un contador global, para saber la última id

    // constructor
    public function __construct($id, $nombre, $email, $usuario, $password, $dni, $foto) {
        $this->id = $id;
        $this->nombre = $nombre;
        $this->email = $email;
        $this->usuario = $usuario;
        $this->password = $password;
        $this->dni = $dni;
        $this->foto = $foto;

        if ($id > self::$cont) // si la id global es menor que la nueva id
            self::$cont = $id; // actualizamos el valor de la id global
    }

    // métodos
    public function getId() {
        return $this->id;
    }

    public function setId($id) {
        $this->id = $id;
    }

    public function getNombre() {
        return $this->nombre;
    }

    public function setNombre($nombre) {
        $this->id = $nombre;
    }

    public function getEmail() {
        return $this->email;
    }
    
    public function setEmail($email) {
        $this->id = $email;
    }
    
    public function getUsuario() {
        return $this->usuario;
    }

    public function setUsuario($usuario) {
        $this->id = $usuario;
    }
    
    public function getPassword() {
        return $this->password;
    }

    public function setPassword($password) {
        $this->id = $password;
    }

    public function getDNI() {
        return $this->dni;
    }
    
    public function setDNI($dni) {
        $this->id = $dni;
    }

    public function getFoto() {
        return $this->foto;
    }
    
    public function setFoto($foto) {
        $this->id = $foto;
    }

    public function getCSV() {
		return $this->getID().";".$this->getNombre().";".$this->getEmail().";".$this->getUsuario().";".$this->getPassword().";".$this->getDNI().";".$this->getFoto();
	}

	public static function getUltimaID() {
		return self::$cont;
	}
}
?>