<?php
class Pedido {
  	// propiedades
    private $id;
    private $fecha;
    private $idCliente;
    private $Notas;

    public static $cont = 0; // será un contador global, para saber la última id

    // constructor
    public function __construct($id, $fecha, $idCliente, $Notas) {
        $this->id = $id;
        $this->fecha = $fecha;
        $this->idCliente = $idCliente;
        $this->Notas = $Notas;

        if ($id > self::$cont) // si la id global es menor que la nueva id
            self::$cont = $id; // actualizamos el valor de la id global
    }

    // métodos
    public function getId(){
		return $this->id;
	}

	public function setId($id){
		$this->id = $id;
	}

	public function getFecha(){
        return $this->fecha;
	}

	public function setFecha($fecha){
		$this->fecha = $fecha;
	}

	public function getIdCliente(){
		return $this->idCliente;
	}

	public function setIdCliente($idCliente){
		$this->idCliente = $idCliente;
  }

  public function getNotas(){
		return $this->Notas;
	}

	public function setNotas($notas){
		$this->Notas = $notas;
  }

  public function getCSV() {
		return $this->getID().";".$this->getFecha().";".$this->getIdCliente().";".$this->getNotas();
	}

	public static function getUltimaID() {
		return self::$cont;
	}
}