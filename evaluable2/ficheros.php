<?php
error_reporting(E_ALL); // mostraremos los posibles errores
ini_set('display_errors', '1');

// incluímos los ficheros de clases
include_once("cliente.php");
include_once("pedido.php");
include_once("articulo.php");
include_once("linea.php");

// algunas variables globales, como nombres de archivo
$dir = "./datos/";
$f_clientes = $dir."clientes.csv";
$f_pedidos = $dir."pedidos.csv";
$f_articulos = $dir."articulos.csv";
$f_lineas = $dir."lineas.csv";

// funciones generales

function archivoReadAll($fichero) { // devuelve un array de líneas de archivo
    $array = array();
    
    $archivo = fopen($fichero, "r"); // abrimos el archivo en modo lectura
    if ($archivo) {
        while (($fila = fgetcsv($archivo, 1000,";")) !== false) // si no hay ningún error, añadimos la fila al array
            $array[] = $fila;
        fclose($archivo); // cerramos archivo
    } else
        return null; // devolvemos null si no se ha podido abrir el archivo
    sort($array); // ordenamos el array antes de devolverlo, así su última Id siempre será la última posición
    return $array; 
}

function archivoCreate($fichero, $linea) { // añadimos una línea al final del archivo
    $archivo = fopen($fichero, "a"); // abrimos el archivo en modo lectura
    if ($archivo)
        fwrite($archivo, $linea.PHP_EOL); // añadimos la línea
    fclose($archivo); // cerramos el archivo
}

function archivoRead($fichero, $id) { // leemos un archivo y devolvemos el objeto con la ID indicada
    $archivo = fopen($fichero, "r"); // abrimos el archivo en modo lectura
    if ($archivo) {
        while (($fila = fgetcsv($archivo, 1000,";")) !== false) { // leemos cada línea CVS
            if ($fila[0] == $id)
                return $fila; // si encuentra la ID, devolvemos toda la línea para trabajar con ella
        }
    }
    return null; // si no encuentra la ID, devolvemos null
}

function archivoDelete($fichero, $id) { // borramos un objeto de un archivo según una ID dada
    $nuevo = array();
    $array = archivoReadAll($fichero); // leemos todos los objetos de ese archivo
    if ($array) { // si no ha habido problemas
        foreach ($array as $linea) {
            if ($linea[0] != $id)
                $nuevo[] = $linea; // añadimos las líneas que no tengan la ID del objeto
        }
        archivoSave($fichero, $nuevo); // guardamos el array sin el objeto indicado
        return true; // devolvemos verdadero si todo ha salido bien
    } else
        return null; // devolvemos null si ha habido problemas con el archivo
}

function archivoSave($fichero, $array) { // guardamos todos las líneas de un array de objetos en el archivo indicado
    $archivo = fopen($fichero, "w");
    if ($archivo) {
        if (count($array) == 0) $array = []; // si el array está vacío daría error, así no muestra nada
        foreach($array as $linea) {
            // si es un objeto guarda con su método getCSV(), si no, es que es un array y guarda la línea entera
            if (is_object($linea))
                $linea = explode(";",$linea->getCSV());
            fputcsv($archivo, $linea, ";"); }
        return true;
    } else
        return null; // devolvemos null si ha encontrado un error al abrir el fichero
}

function archivoUpdate($fichero, $objeto) {
    $nuevo = array();
    $array = archivoReadAll($fichero); // leemos todos los objetos de ese archivo
    if ($array) { // si no ha habido problemas
        foreach ($array as $linea) {
            if ($linea[0] == $objeto->getID())
                $nuevo[] = explode(";",$objeto->getCSV()); // si hemos encontrado la ID que interesa, la cambiamos por el nuevo objeto
            else
                $nuevo[] = $linea; // añadimos las líneas que no tengan la ID del objeto
        }
        archivoSave($fichero, $nuevo); // guardamos el array sin el objeto indicado
        return true; // devolvemos verdadero si todo ha salido bien
    } else
        return null; // devolvemos null si ha habido problemas con el archivo
}


// funciones artículos
function ArticuloReadAll() { // devuelve un vector de objetos artículos
    global $f_articulos;
    $array = array();
    $lineas = archivoReadAll($f_articulos); // leemos todas las líneas del archivo en formato CVS
    if ($lineas) { // si no ha devuelto null
        foreach ($lineas as $arry => $campo)
            $array[] = new Articulo($campo[0],$campo[1],$campo[2],$campo[3]); // añadimos el objeto creado al array
        return $array; // devolvemos el array con todos los objetos
    }
    return null; // si no, devolvemos null
}

function ArticuloCreate($articulo) { // para crear el objeto articulo en el fichero
    global $f_articulos;
    archivoCreate($f_articulos, $articulo->getCSV());
}

function ArticuloRead($id) { // para leer el articulo con ese id. Devuelve el objeto del fichero
    global $f_articulos;
    $fila = archivoRead($f_articulos, $id);
    
    return ($fila) ? new Articulo($fila[0],$fila[1],$fila[2],$fila[3]) : null; // devolvemos el objeto encontrado o null
}

function ArticuloUpdate($articulo) { // para actualizar el objeto articulo en un fichero
    global $f_articulos;
    return archivoUpdate($f_articulos, $articulo);
}

function ArticuloDelete($id) { // para borrar el articulo
    global $f_articulos;
    return archivoDelete($f_articulos, $id);
}

// funciones clientes
function ClienteReadAll() { // devuelve un vector de objetos artículos
    global $f_clientes;
    $array = array();
    $lineas = archivoReadAll($f_clientes); // leemos todas las líneas del archivo en formato CVS
    if ($lineas) { // si no ha devuelto null
        foreach ($lineas as $arry => $campo)
            // añadimos el objeto creado al array
            $array[] = new Cliente($campo[0],$campo[1],$campo[2],$campo[3],$campo[4],$campo[5],$campo[6]);
        return $array; // devolvemos el array con todos los objetos
    }
    return null; // si no, devolvemos null
}

function ClienteCreate($cliente) { // para crear el objeto cliente en el fichero
    global $f_clientes;
    archivoCreate($f_clientes, $cliente->getCSV());
}

function ClienteRead($id) { // para leer el cliente con ese id. Devuelve el objeto del fichero
    global $f_clientes;
    $fila = archivoRead($f_clientes, $id);
    // devolvemos el objeto encontrado o null
    return ($fila) ? new Cliente($fila[0],$fila[1],$fila[2],$fila[3],$fila[4],$fila[5],$fila[6]) : null;
}

function ClienteUpdate($cliente) { // para actualizar el objeto cliente en un fichero
    global $f_clientes;
    return archivoUpdate($f_clientes, $cliente);
}

function ClienteDelete($id) { // para borrar el cliente
    global $f_clientes;
    return archivoDelete($f_clientes, $id);
}

// funciones pedidos
function PedidoReadAll() { // devuelve un vector de objetos artículos
    global $f_pedidos;
    $array = array();
    $lineas = archivoReadAll($f_pedidos); // leemos todas las líneas del archivo en formato CVS
    if ($lineas) { // si no ha devuelto null
        foreach ($lineas as $arry => $campo)
            // añadimos el objeto creado al array
            $array[] = new Pedido($campo[0],$campo[1],$campo[2],$campo[3]);
        return $array; // devolvemos el array con todos los objetos
    }
    return null; // si no, devolvemos null
}

function PedidoCreate($pedido) { // para crear el objeto pedido en el fichero
    global $f_pedidos;
    archivoCreate($f_pedidos, $pedido->getCSV());
}

function PedidoRead($id) { // para leer el pedido con ese id. Devuelve el objeto del fichero
    global $f_pedidos;
    $fila = archivoRead($f_pedidos, $id);
    // devolvemos el objeto encontrado o null
    return ($fila) ? new Pedido($fila[0],$fila[1],$fila[2],$fila[3]) : null;
}

function PedidoUpdate($pedido) { // para actualizar el objeto pedido en un fichero
    global $f_pedidos;
    return archivoUpdate($f_pedidos, $pedido);
}

function PedidoDelete($id) { // para borrar el pedido
    global $f_pedidos;
    return archivoDelete($f_pedidos, $id);
}

// funciones líneas
function LineaReadAll() { // devuelve un vector de objetos artículos
    global $f_lineas;
    $array = array();
    $lineas = archivoReadAll($f_lineas); // leemos todas las líneas del archivo en formato CVS
    if ($lineas) { // si no ha devuelto null
        foreach ($lineas as $arry => $campo)
            // añadimos el objeto creado al array
            $array[] = new Linea($campo[0],$campo[1],$campo[2],$campo[3],$campo[4],$campo[5]);
        return $array; // devolvemos el array con todos los objetos
    }
    return null; // si no, devolvemos null
}

function LineaCreate($linea) { // para crear el objeto linea en el fichero
    global $f_lineas;
    archivoCreate($f_lineas, $linea->getCSV());
}

function LineaRead($id) { // para leer el linea con ese id. Devuelve el objeto del fichero
    global $f_lineas;
    $fila = archivoRead($f_lineas, $id);
    // devolvemos el objeto encontrado o null
    return ($fila) ? new Linea($fila[0],$fila[1],$fila[2],$fila[3],$fila[4],$fila[5]) : null;
}

function LineaUpdate($linea) { // para actualizar el objeto linea en un fichero
    global $f_lineas;
    return archivoUpdate($f_lineas, $linea);
}

function LineaDelete($id) { // para borrar el linea
    global $f_lineas;
    return archivoDelete($f_lineas, $id);
}

?>