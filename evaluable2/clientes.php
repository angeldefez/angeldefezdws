<!DOCTYPE html>
<html>

<head>
    <link rel="stylesheet" type="text/css" href="estilos.css">
    <meta charset="utf-8">
</head>

<body>
    <div>
    <h1>Clientes</h1>
    <h2>Menú</h2>

    <?php
    include("util.php");

    $campos = array("ID","Nombre","Email","Usuario","Password","DNI","Foto"); // los campos que contiene tanto el formulario como la tabla
    global $f_clientes;
    $fichero = $f_clientes;

    // interceptamos la opción elegida y mostramos un menú u otro, según convenga
    if ($op == "r") // read
        echo '<img class="foto" src="imagenes/'.ClienteRead($id)->getFoto().'" alt="Foto de cliente" height="100" width="100">';
    if ($op=="e") { // resultado de un formulario
        $array = validarDatos($campos);
        // hay campos que es mejor validarlos a mano
        $array["Password"] = (limpiaCampo("password1") == limpiaCampo("password2") ? limpiaCampo("password1") : null);
        $array["Foto"] = obtenerFoto("Foto","imagenes/");
        procesaResultado($array);
    }
    else
        mostrarMenu(); // mostramos el menú principal

    ?>
    <span class="limpia"></span>
    <footer><p>Evaluable 1 DWES | Ángel de Fez | 13-11-2019</p></footer>
    </div>
    <?php
    // muestra el menú de cada página. al tratarse de objetos, cada uno tiene su propio menú
    function mostrarMenu() {
        global $op, $url_actual, $id, $usuario, $campos;
        $objetos = ClienteReadAll();
        switch ($op) {
            case "c":
                $nuevaID = Cliente::getUltimaID()+1;
                mostrarFormulario($nuevaID, $op);
                crearEnlace($url_actual."?usuario=$usuario","Volver","derecha");
                break;
            case "r":
                crearEnlace($url_actual."?op=c&usuario=$usuario","Crear nuevo","derecha");
                $o[]= ClienteRead($id);
                muestraTabla($o, $campos, false);
                crearEnlace($url_actual."?usuario=$usuario","Volver","derecha");
                break;
            case "u":
                mostrarFormulario($id, $op);
                crearEnlace($url_actual."?usuario=$usuario","Volver","derecha");
                break;
            case "d":
                ClienteDelete($id);
                $objetos = ClienteReadAll(); // actualizamos los datos
                crearEnlace($url_actual."?op=c&usuario=$usuario","Crear nuevo","derecha");
                muestraTabla($objetos, $campos, true);
                break;
            case "e": // resultado de un formulario
                    $tabla = array();
                    $tabla["ID"] = (isset($_REQUEST["ID"])) ? $_REQUEST["ID"] : null;
                    $tabla["Nombre"] = (isset($_REQUEST["Nombre"])) ? $_REQUEST["Nombre"] : null;
                    $tabla["Email"] = (isset($_REQUEST["Email"])) ? $_REQUEST["Email"] : null;
                    $tabla["Usuario"] = (isset($_REQUEST["Usuario"])) ? $_REQUEST["Usuario"] : null;
                    $tabla["DNI"] = (isset($_REQUEST["DNI"])) ? $_REQUEST["DNI"] : null;
                    $array = validarDatos($tabla, $campos);
                    $array["Password"] = (limpiaCampo("password1") == limpiaCampo("password2") ? limpiaCampo("password1") : null);
                    $array["Foto"] = obtenerFoto("Foto","imagenes/");
                    procesaResultado($tabla, $array);
                    break;
            default:
                crearEnlace($url_actual."?op=c&usuario=$usuario","Crear nuevo","derecha");
                muestraTabla($objetos, $campos, true);
        }
        crearEnlace("index.php?usuario=$usuario","Inicio","derecha");
    }
        // muestra el formulario, cada tabla tiene el suyo propio
        function mostrarFormulario($id, $op) {
            $objeto = ClienteRead($id);
            $nombre = ($objeto) ? $objeto->getNombre() : null; // si existe el objeto, llenamos los campos
            $email = ($objeto) ? $objeto->getEmail() : null;
            $password = ($objeto) ? $objeto->getPassword() : null;
            $user = ($objeto) ? $objeto->getUsuario() : null;
            $dni = ($objeto) ? $objeto->getDNI() : null;
            $foto = ($objeto) ? $objeto->getFoto() : null;

            global $usuario;
            $titulo = ($op == 'c') ? "Crear Nuevo":"Modificar";
        
            echo '<form name="formulario" method="post" action="clientes.php?op=e&id='.$id.'&usuario='.$usuario.'" enctype="multipart/form-data">';
            echo '<fieldset>';
            echo '<legend>'.$titulo.' Cliente</legend>';
            echo '<img class="form" src="imagenes/'.$foto.'" alt="Foto de cliente" height="100" width="100">';
            echo '<p>ID: <input type="text" name="ID" size="5" required readonly value='.$id.'></p>'; // sólo lectura, no se puede modificar
            echo '<p>Nombre y apellidos: <input type="text" name="Nombre" size="50" required value="'.$nombre.'"></p>';
            echo '<p>E-mail: <input type="text" name="Email" size="30" required value="'.$email.'"></p>';
            echo '<p>Nombre de usuario: <input type="text" name="Usuario" size="20" required value="'.$user.'"></p>';
            echo '<p>DNI: <input type="text" name="DNI" size="20" required value="'.$dni.'"></p>';
            echo '<p>Contraseña: <input type="password" name="password1" size="20" required value="'.$password.'"> Introduce de 7 a 20 caráceres</p>';
            echo '<p>Repite contraseña: <input type="password" name="password2" size="20" required value="'.$password.'"></p>';
            echo '<p>Foto: <input type="file" name="Foto" value="'.$foto.'"/></p>';
            echo '<input type="hidden" name="max_file_size" value="102400" >';
            echo '<input name="o" type="hidden" value="'.$op.'">';
            echo '<input type="submit" value="Enviar">';
            echo '<input type="reset" value="Reiniciar">';
            echo '</fieldset>';
            echo '</form>';
        }

        function crearNuevo($tabla) {
            ClienteCreate(new Cliente($tabla["ID"],$tabla["Nombre"],$tabla["Email"],$tabla["Usuario"],$tabla["Password"],$tabla["DNI"],$tabla["Foto"]));
        }
    
        function actualizarTabla($tabla) {
            ClienteUpdate(new Cliente($tabla["ID"],$tabla["Nombre"],$tabla["Email"],$tabla["Usuario"],$tabla["Password"],$tabla["DNI"],$tabla["Foto"]));
        }
    ?>
</body>
</html>