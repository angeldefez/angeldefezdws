<?php
class Linea {
	// propiedades
    private $id;
    private $cantidad;
    private $fecha;
    private $idPedido;
    private $idCliente;
    private $idProducto;

    public static $cont = 0; // será un contador global, para saber la última id

    // constructor
    public function __construct($id, $cantidad, $fecha, $idPedido, $idCliente, $idProducto) {
        $this->id = $id;
        $this->cantidad = $cantidad;
        $this->fecha = $fecha;
        $this->idPedido = $idPedido;
        $this->idCliente = $idCliente;
        $this->idProducto = $idProducto;

        if ($id > self::$cont) // si la id global es menor que la nueva id
            self::$cont = $id; // actualizamos el valor de la id global
    }

    // métodos
	public function getId(){
		return $this->id;
	}

	public function setId($id){
		$this->id = $id;
	}

	public function getCantidad(){
		return $this->cantidad;
	}

	public function setCantidad($cantidad){
		$this->cantidad = $cantidad;
	}

	public function getFecha(){
		return $this->fecha;
	}

	public function setFecha($fecha){
		$this->fecha = $fecha;
	}

	public function getIdPedido(){
		return $this->idPedido;
	}

	public function setIdPedido($idPedido){
		$this->idPedido = $idPedido;
	}

	public function getIdCliente(){
		return $this->idCliente;
	}

	public function setIdCliente($idCliente){
		$this->idCliente = $idCliente;
	}

	public function getIdProducto(){
		return $this->idProducto;
	}

	public function setIdProducto($idProducto){
		$this->idProducto = $idProducto;
	}

	public function getCSV() {
		return $this->getID().";".$this->getCantidad().";".$this->getFecha().";".$this->getIdPedido().";".$this->getIdCliente().";".$this->getIdProducto();
	}

	public static function getUltimaID() {
		return self::$cont;
	}
}