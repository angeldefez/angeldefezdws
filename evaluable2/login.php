<!DOCTYPE html>
<html>

<head>
    <link rel="stylesheet" type="text/css" href="estilos.css">
    <meta charset="utf-8">
</head>
<?php
    include("util.php");

    $usuario = limpiaCampo("usuario");
    $password = limpiaCampo("password");

    if (!$usuario)
        mostrarLogin();
    else {

        $usuarios = archivoReadAll("datos/clientes.txt");
        $pass = null;
        foreach($usuarios as $u) {
            if ($u[3] == $usuario) { // el cuarto campo es el nombre de usuario
                $pass = $u[4]; // el quinto campo es el password
                break;
            }
        }

        if ($pass == $password) { // el login es correcto
            // copiamos la base de datos base a los archivos CSV
            archivoSave("datos/clientes.csv", archivoReadAll("datos/clientes.txt")); // creamos el archivo de clientes
            archivoSave("datos/articulos.csv", archivoReadAll("datos/articulos.txt")); // creamos el archivo de articulos
            archivoSave("datos/pedidos.csv", archivoReadAll("datos/pedidos.txt")); // creamos el archivo de pedidos
            archivoSave("datos/lineas.csv", archivoReadAll("datos/lineas.txt")); // creamos el archivo de líneas de pedidos

            header("location:index.php?usuario=$usuario"); // re-dirigimos al índice, con el usuario como parámetro
        }
        else {
            echo "<div class='login'>";
            echo "<h3>Error, usuario o contraseña incorrectos</h3>";
            crearEnlace("login.php","Volver","derecha");
            echo "</div>";
        }
    }
?>
<body>
<?php
    // pantalla de login si no hay usuario activo
    function mostrarLogin() {
        echo '<div class="login"><form action="login.php" method="post">';
        echo '<input name="usuario" type="text" placeholder="Nombre de usuario" value="pacoaldarias">';
        echo '<br><br>';
        echo '<input name="password" type="password" placeholder="Contraseña" value="1234">';
        echo '<br><br>';
        echo '<input type="submit" value="Iniciar sesión"></form>';
        echo '<br><br>';
        echo '<abbr title="Usuario: pacoaldarias Contraseña: 1234">Ayuda</abbr>';
        echo '</div>';
    }
?>
</body>
</html>