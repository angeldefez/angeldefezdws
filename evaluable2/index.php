<!DOCTYPE html>
<html>

<head>
    <link rel="stylesheet" type="text/css" href="estilos.css">
    <meta charset="utf-8">
</head>
<?php

    $usuario=(isset($_GET["usuario"])) ? $_GET["usuario"] : null; // obtenemos el código id de la url, de haberlo

    if (!$usuario)
        header("location:login.php");

?>
<body>
    <div>
        <h1>Inicio</h1>
        <h2>Menú</h2>
    <ul class="menu">
        <li><a href="articulos.php<?php echo "?usuario=$usuario"?>">Artículos</a></li>
        <li><a href="clientes.php<?php echo "?usuario=$usuario"?>">Clientes</a></li>
        <li><a href="pedidos.php<?php echo "?usuario=$usuario"?>">Pedidos</a></li>
    </ul>
    <h2>Información</h2>
    <ul class="menu">
        <li><a href="enunciado.html">Enunciado de la práctica</a></li>
        <li><a href="documentacion.html">Documentación del alumno</a></li>
        <li><a href="https://bitbucket.org/angeldefez/angeldefezdws">BitBucket alumno</a></li>
        <li><a href="https://angeldefezdws.herokuapp.com">Heroku alumno</a></li>
    </ul>
    <a class="derecha" href="../index.html">Volver</a>
    <footer><p>Evaluable 1 DWES | Ángel de Fez | 13-11-2019</p></footer>
</div>
</body>
</html>