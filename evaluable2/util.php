<?php
include_once("producto.php"); // incluímos otros ficheros importantes
include_once("ficheros.php");
include("formularios.php");

// variables globales

$op=(isset($_GET["op"])) ? $_GET["op"] : null; // obtenemos el código de la operación de la url, de haberlo
$id=(isset($_GET["id"])) ? $_GET["id"] : null; // obtenemos el código id de la url, de haberlo
$usuario=(isset($_GET["usuario"])) ? $_GET["usuario"] : null; // obtenemos el código id de la url, de haberlo
$url_anterior = (isset($_SERVER['HTTP_REFERER'])) ? $_SERVER['HTTP_REFERER'] : null;
$url_actual = $_SERVER['PHP_SELF'];

$botones_urls=array("?op=r","?op=u","?op=d"); // las opciones de los botonos de la tabla
$botones_texto=array("Ver","Modificar","Borrar"); // la etiqueta de cada botón de la tabla

// funciones generales

// convierte un texto separado por el carácter especificado a una serie de <td>
function CVSaTabla($cadena, $simbolo = ";") {
    $texto = "";
    $array = explode($simbolo, $cadena); // dividimos por el carácter seleccionado, obteniendo un array
    foreach ($array as $valor)
        $texto .= "<td>$valor</td>";
    
    return $texto;
}

// crea una tabla a partir del array de objetos, el nombre de los campos y si quiere o no mostrar botones de enlaces
function muestraTabla($objetos, $campos, $botones = false) {
    echo "<table><tr>".creaCabecera($campos, $botones)."</tr>";
    if (empty($objetos))
        $objetos[] = $objetos; // si el array está vacío daría error, así no muestra nada
    foreach($objetos as $o) {
        echo "<tr>".CVSaTabla($o->getCSV());
        if ($botones)
            echo creaBotones($o->getId())."</tr>";
    }
    echo "</table>";
}

// crea enlaces para añadir a una tabla para hacer el CRUD
function creaBotones($id, $texto = null) {
    global $botones_texto, $botones_urls, $url_actual, $usuario;
    $link="";
    for ($i=0; $i<count($botones_urls); $i++) {
        $url = $url_actual.$botones_urls[$i]."&id=".$id."&usuario=".$usuario."$texto";
        $link .= "<a href='$url'>$botones_texto[$i]</a>";
    }
    return "<td>$link</td>";
}

// crea los <th> de una tabla según los campos dados
function creaCabecera($campos, $botones = false) {
    $texto = "";
    foreach ($campos as $v)
        $texto .= "<th>$v</th>";
    if ($botones)
        $texto .= '<th>Acciones</th>';
    return $texto;
}

function crearEnlace($url, $texto, $clase = false) {
    echo "<a ".(($clase) ? "class='$clase'" : "")."href='$url'>$texto</a>";
}

// imprime todos los elementos de un array separados por saltos <br>
function imprimirArray($array) {
    foreach ($array as $i)
        echo $i."<br>";
}

// muestra una tabla según el contenido del array, con cada clave como encabezado de la misma
function arrayAtabla($array, $tabla = false) {
    $multi = isset($array[0]); // comprueba si el array es multidimensional o no
    if ($tabla) echo "<table>";
    echo "<tr>";
    $cabecera = ($multi) ? $array[0] : $array; //si es un array multidimensional necesitamos elegir 1 elemento
    foreach($cabecera as $clave => $valor){
        echo "<th>$clave</th>";
    }
    echo "</tr>";
    foreach($array as $clave) {
        if ($multi) { // si es un array múltiple, hay que mostrarlo junto a <tr></tr>
            echo "<tr>";
            foreach($clave as $valor){
                echo "<td>{$valor}</td>";
            }
            echo "</tr>";
        } else { // si no es un array múltiple, mostramos su único valor
            echo "<td>{$clave}</td>";
        }
    }
    if ($tabla) echo "</table>";
}

?>