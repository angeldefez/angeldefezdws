<?php
class Articulo {
	// propiedades
    private $id;
    private $nombre;
    private $precio;
    private $descripcion;

    public static $cont = 0; // será un contador global, para saber la última id

    // constructor
    public function __construct($id, $nombre, $precio, $descripcion) {
        $this->id = $id;
        $this->nombre = $nombre;
        $this->precio = $precio;
        $this->descripcion = $descripcion;

        if ($id > self::$cont) // si la id global es menor que la nueva id
            self::$cont = $id; // actualizamos el valor de la id global
    }

    // métodos
	public function getId(){
		return $this->id;
	}

	public function setId($id){
		$this->id = $id;
	}

	public function getNombre(){
		return $this->nombre;
	}

	public function setNombre($nombre){
		$this->nombre = $nombre;
	}

	public function getPrecio(){
		return $this->precio;
	}

	public function setPrecio($precio){
		$this->precio = $precio;
	}

	public function getDescripcion(){
		return $this->descripcion;
	}

	public function setDescripcion($descripcion){
		$this->descripcion = $descripcion;
	}

	public function getCSV() {
		return $this->getID().";".$this->getNombre().";".$this->getPrecio().";".$this->getDescripcion();
	}

	public static function getUltimaID() {
		return self::$cont;
	}
}