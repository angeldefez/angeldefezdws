<?php

// función de desinfección de código, compartida por el profesor
function limpiaCampo($campo) {
    $valor = false;
    if (isset($_REQUEST[$campo]))
        $valor = strip_tags(trim(htmlspecialchars($_REQUEST[$campo])));
    return $valor;
}

function generaPassword($n) {
    $car = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ'.'abcdefghijklmnopqrstuvwxyz'.'0123456789'.'!_"@$-~&.*';
    $pass = "";
    for ($i=0; $i<$n; $i++)
        $pass .= $car[rand(0,strlen($car)-1)];
    return $pass;
}

// valida los datos de un array según su nombre de clave
function validarDatosArray($array) {
    $error = []; // para mostrar los posibles errores

    foreach ($array as $clave => $valor) {
        switch ($clave) {
            case "Email":
                if (!validaEmail($valor))
                    $error[] = $clave." incorrecto";
                break;
            case "DNI":
                if (!validaDNI($valor))
                    $error[] = $clave." incorrecto. Introduzca 7 u 8 dígitos y una letra. Puede ir separada por un guión.";
                break;
            case "Password":
                if (!validaPassword($valor))
                    $error[] = $clave." incorrecto. Solo se permiten de 7 a 20 letras, números y los siguientes 
                    caracteres: !_\"@$~&\.*";
                break;
            case "Fecha":
                if (!validaFecha($valor))
                    $error[] = $clave." incorrecta. Se permiten dd/mm/aaaa dd-mm-aaaa y dd.mm.aaaa";
                break;
            case "Cantidad":
            case "ID":
            case "idCliente":
                if (!validaEntero($valor))
                    $error[] = $clave." incorrecto. No es un número entero";
                break;
            case "Precio":
                if (!validaDecimal($valor))
                $error[] = $clave." incorrecto. No es un número válido";
        break;
            default:
        }
    }

    return $error;
}

// función para procesar el resultado del formulario
function procesaResultado($array) {
    $error = validarDatosArray($array);
    global $op, $id, $url_actual, $usuario;

    $op = limpiaCampo("o");
    global $op, $id;
    if (count($error) > 0) { // si error tiene algún dato es que ha fallado algo en la validación, lo mostramos
        echo "<h3>Error al validar el formulario</h3>";
        $m="";
        foreach ($error as $e)
            $m .= "$e<br><br>";
        $m = substr($m, 0, -4); // borramos el último <br>
        mensajeError($m, "$url_actual?op=$op&id=$id");
    } else {
        echo "<h3>Datos correctos</h3>";

        if ($op == "u") // si es update
            $e = actualizarTabla($array); // actualizamos la tabla y recogemos el posible error
        else  // si es create
            $e = crearNuevo($array); // insertamos un nuevo elemento y recogemos el posible error

        if ($e) { // si todo ha salido bien
            mensajeError("Se ha actualizado la base de datos correctamente<br>", "$url_actual","Volver","login inform");
            return true; // decimos que la operación ha salido bien
        } else { // si no...
            mensajeError("Ha surgido un error actualizando la base de datos. La consulta ha devuelto: <br>$e", "$url_actual");
            return false; // decimos que la no ha funcionado
        }
    }
}

// función muy útil para añadir o modificar elementos dentro de la tabla
function validarDatos($campos) {
    $array = []; // almacenaremos un array para enviarlo y añadirlo a la tabla correspondiente
    foreach ($campos as $i) // recorremos todos los campos y añadimos al array el valor pertinente
        $array[$i] = limpiaCampo($i); // hacemos un REQUEST de cada campo, limpiando el texto previamente
    return $array;
}

function obtenerFoto($campo, $target_path) {
    $target_path = $target_path . basename($_FILES[$campo]['name']);
    if (move_uploaded_file($_FILES[$campo]['tmp_name'], $target_path))
        return basename($_FILES[$campo]['name']);
    else
        return null;
}

// validadores
function validaEmail($texto) {
    $reg = "/^\w+@\w+\..{2,3}$/";
    return (preg_match($reg, $texto)) ? $texto : false;
}

function validaDNI($texto) {
    $reg = "/^\d{7,8}-?[a-z]$/i";
    return (preg_match($reg, $texto)) ? $texto : false;
}

function validaPassword($texto) {
    $reg = "/^[0-9a-z!_\"@$\-~&.*]{7,20}$/i";
    return (preg_match($reg, $texto)) ? $texto : false;
}
function validaFecha($texto) {
    $reg = "/^([0-3])?[0-9]{1}(\/|\-|\.){1}([0-2])?[0-9]{1}(\/|\-|\.){1}[0-9]{2,4}$/";
    return (preg_match($reg, $texto)) ? $texto : false;
}

function validaEntero($texto) {
    $reg = "/^\d+$/";
    return (preg_match($reg, $texto)) ? $texto : false;
}

function validaDecimal($texto) {
    $reg = "/^\d+(\.\d+)?$/";
    return (preg_match($reg, $texto)) ? $texto : false;
}