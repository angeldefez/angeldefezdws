<?php
    interface IDatos {
        
    // funciones generales
    // function ReadAll(); // devuelve un array de líneas de la fuente de datos
    // function Create($linea); // añadimos una línea al final de la fuente de datos
    // function Read($id); // leemos de la fuente de datos y devolvemos el objeto con la ID indicada
    // function Delete($id); // borramos un objeto de la fuente de datos según una ID dada
    // function Save($array); // guardamos todos las líneas de un array de objetos en la fuente de datos
    // function Update($objeto);

    // funciones artículos
    function ArticuloReadAll(); // devuelve un vector de objetos artículos
    function ArticuloCreate($articulo); // para crear el objeto articulo en la fuente de datos
    function ArticuloRead($id); // para leer el articulo con ese id. Devuelve el objeto de la fuente de datos
    function ArticuloUpdate($articulo); // para actualizar el objeto articulo en un fichero
    function ArticuloDelete($id); // para borrar el articulo

    // funciones clientes
    function ClienteReadAll(); // devuelve un vector de objetos clientes
    function ClienteCreate($cliente); // para crear el objeto cliente en la fuente de datos
    function ClienteRead($id); // para leer el cliente con ese id. Devuelve el objeto de la fuente de datos
    function ClienteUpdate($cliente); // para actualizar el objeto cliente en un fichero
    function ClienteDelete($id); // para borrar el cliente

    // funciones pedidos
    function PedidoReadAll(); // devuelve un vector de objetos pedidos
    function PedidoCreate($pedido); // para crear el objeto pedido en la fuente de datos
    function PedidoRead($id); // para leer el pedido con ese id. Devuelve el objeto de la fuente de datos
    function PedidoUpdate($pedido); // para actualizar el objeto pedido en un fichero
    function PedidoDelete($id); // para borrar el pedido

    // funciones líneas
    function LineaReadAll(); // devuelve un vector de objetos líneas
    function LineaCreate($linea); // para crear el objeto linea en la fuente de datos
    function LineaRead($id); // para leer el linea con ese id. Devuelve el objeto de la fuente de datos
    function LineaUpdate($linea); // para actualizar el objeto linea en un fichero
    function LineaDelete($id); // para borrar el linea
    }
?>