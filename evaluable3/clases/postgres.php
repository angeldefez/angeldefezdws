<?php
error_reporting(E_ALL); // mostraremos los posibles errores
ini_set('display_errors', '1');

// incluímos los ficheros de clases
include_once("clases/config.php"); // incluímos la clase estática Config, que tiene la configuración de la BBDD
include_once("clases/cliente.php");
include_once("clases/pedido.php");
include_once("clases/articulo.php");
include_once("clases/linea.php");

class Postgres implements IDatos {

    protected $db;

    public function __construct()
    {
        $this->db = new PDO(
            'pgsql:host='.Config::$bdhostname.
            ';dbname='.Config::$bdnombre,
            // ';charset=utf8',
            Config::$bdusuario,
            Config::$bdclave);
    }
    // funciones generales

    function close() {
        $this->db = null; // desconectamos la base de datos
    }

    function ReadAll($tabla, $parametros = null) { // devuelve un array de columnas de la tabla seleccionada
        $consulta = "SELECT * FROM $tabla ORDER BY id ASC";
        $result = $this->db->prepare($consulta); // preparamos la consulta
        if ($parametros) 
            $result->execute($parametros); // la ejecutamos, si tiene parámetros 
        else
            $result->execute(); // ...o sencilla, si no tiene parámetros
       
        if ($result) // si la consulta ha devuelto algo...
            return $result->fetchAll(PDO::FETCH_OBJ); // devuelve un array de objetos
        else
            return null; // o no devuelve nada si la consulta está vacía
    }

    function Create($consulta, $parametros){ // añadimos una línea al final del archivo
        $result = $this->db->prepare($consulta); // preparamos la consulta
        $r = $result->execute($parametros); // ejecutamos la consulta con parámetros

        if(!$r) {
            print_r($this->db->errorInfo());
        }

        return $r; // devuelve la consulta, ya sea correcta o no
    }

    function Read($tabla, $id){ // leemos un archivo y devolvemos el objeto con la ID indicada
        $consulta = "SELECT * FROM $tabla WHERE id=:id";
        $result = $this->db->prepare($consulta); // preparamos la consulta
        $parametros = array(":id"=>$id); // definimos los parámetros, en este caso la ID

        $result->execute($parametros); // ejecutamos la consulta con parámetros 

        if ($result) // si la consulta ha devuelto algo...
            return $result->fetch(PDO::FETCH_OBJ); // devuelve un único objeto
        else
            return null; // o no devuelve nada si la consulta está vacía
    }
    function Delete($tabla, $id){ // borramos un objeto de una tabla según una ID dada
        $consulta = "DELETE FROM $tabla WHERE id=:id";
        $result = $this->db->prepare($consulta); // preparamos la consulta
        $parametros = array(":id"=>$id);
        $result->execute($parametros); // ejecutamos la consulta con parámetros

        return $result; // devuelve la consulta, ya sea correcta o no
    }

    function Update($consulta, $parametros){
        $result = $this->db->prepare($consulta); // preparamos la consulta
        $result->execute($parametros); // ejecutamos la consulta con parámetros

        return $result; // devuelve la consulta, ya sea correcta o no
    }

    // funciones artículos
    function ArticuloReadAll(){ // devuelve un vector de objetos artículos
        $filas = $this->ReadAll("articulo"); // hace un SELECT * de la tabla indicada
        $array = [];

        foreach ($filas as $f) 
            $array[] = new Articulo($f->id, $f->nombre, $f->precio, $f->descripcion);

        return $array; // devolvemos el array de objetos encontrados
    }

    function ArticuloCreate($articulo){ // para crear el objeto articulo en el fichero
        $consulta = "INSERT INTO articulo VALUES (:id, :nombre, :precio, :descripcion)";
        $a = explode(";",$articulo->getCSV()); // obtenemos los campos en un array para usarlo de una manera más sencilla
        $parametros = array(
            ":id"=>$a[0],
            ":nombre"=>$a[1],
            ":precio"=>$a[2],
            ":descripcion"=>$a[3]);

        return $this->Create($consulta, $parametros);
    }

    function ArticuloRead($id){ // para leer el articulo con ese id. Devuelve el objeto del fichero
        $f = $this->Read("articulo", $id); // hace un SELECT de la tabla indicada
        // devolvemos el objeto encontrado o null
        return (!$f) ? null : new Articulo($f->id, $f->nombre, $f->precio, $f->descripcion);

    }
    function ArticuloUpdate($articulo){ // para actualizar el objeto articulo en un fichero
        $consulta = "UPDATE articulo SET nombre=:nombre, precio=:precio, descripcion=:descripcion WHERE id=:id";
        $a = explode(";",$articulo->getCSV()); // obtenemos los campos en un array para usarlo de una manera más sencilla
        $parametros = array(
            ":id"=>$a[0],
            ":nombre"=>$a[1],
            ":precio"=>$a[2],
            ":descripcion"=>$a[3]);

        return $this->Update($consulta, $parametros);
    }

    function ArticuloDelete($id){ // para borrar el articulo
        return $this->Delete("articulo", $id); // devolverá el resultado de la consulta DELETE FROM
    }

    // funciones clientes
    function ClienteReadAll(){ // devuelve un vector de objetos clientes
        $filas = $this->ReadAll("cliente"); // hace un SELECT * de la tabla indicada
        $array = [];

        foreach ($filas as $f) 
            $array[] = new Cliente($f->id, $f->nombre, $f->email, $f->usuario, $f->password, $f->dni, $f->foto);
        
        return $array; // devolvemos el array de objetos encontrados
    }

    function ClienteCreate($cliente){ // para crear el objeto cliente en el fichero
        $consulta = "INSERT INTO cliente VALUES (:id, :nombre, :email, :usuario, :password, :dni, :foto)";
        $c = explode(";",$cliente->getCSV()); // obtenemos los campos en un array para usarlo de una manera más sencilla

        $parametros = array(
            ":id"=>$c[0],
            ":nombre"=>$c[1],
            ":email"=>$c[2],
            ":usuario"=>$c[3],
            ":password"=>$c[4],
            ":dni"=>$c[5],
            ":foto"=>$c[6]);

        return $this->Create($consulta, $parametros);
        
    }

    function ClienteRead($id){ // para leer el cliente con ese id. Devuelve el objeto del fichero
        $f = $this->Read("cliente", $id); // hace un SELECT de la tabla indicada
        // devolvemos el objeto encontrado o null
        return (!$f) ? null : new Cliente($f->id, $f->nombre, $f->email, $f->usuario, $f->password, $f->dni, $f->foto);
    }

    function ClienteUpdate($cliente){ // para actualizar el objeto cliente en un fichero
        $consulta = "UPDATE cliente SET nombre=:nombre, email=:email, usuario=:usuario, password=:password, dni=:dni, foto=:foto WHERE id=:id";
        $c = explode(";",$cliente->getCSV()); // obtenemos los campos en un array para usarlo de una manera más sencilla
        $parametros = array(
            ":id"=>$c[0],
            ":nombre"=>$c[1],
            ":email"=>$c[2],
            ":usuario"=>$c[3],
            ":password"=>$c[4],
            ":dni"=>$c[5],
            ":foto"=>$c[6]);

        return $this->Update($consulta, $parametros);
    }

    function ClienteDelete($id){ // para borrar el cliente
        return $this->Delete("cliente", $id); // devolverá el resultado de la consulta DELETE FROM
    }

    // funciones pedidos
    function PedidoReadAll(){ // devuelve un vector de objetos pedidos
        $filas = $this->ReadAll("pedido"); // hace un SELECT * de la tabla indicada
        $array = [];

        foreach ($filas as $f) 
            $array[] = new Pedido($f->id,$f->idCliente,$f->fecha,$f->notas);
        
        return $array; // devolvemos el array de objetos encontrados
    }
    function PedidoCreate($pedido){ // para crear el objeto pedido en el fichero
        $consulta = "INSERT INTO pedido VALUES (:id, :idCliente, :fecha, :notas)";
        $p = explode(";",$pedido->getCSV()); // obtenemos los campos en un array para usarlo de una manera más sencilla
        $parametros = array(
            ":id"=>$p[0],
            ":idCliente"=>$p[1],
            ":fecha"=>$p[2],
            ":notas"=>$p[3]);

        return $this->Create($consulta, $parametros);
    }

    function PedidoRead($id){ // para leer el pedido con ese id. Devuelve el objeto del fichero
        $f = $this->Read("pedido", $id); // hace un SELECT de la tabla indicada
        // devolvemos el objeto encontrado o null
        return (!$f) ? null : new Pedido($f->id,$f->idCliente,$f->fecha,$f->notas);
    }
    function PedidoUpdate($pedido){ // para actualizar el objeto pedido en un fichero
        $consulta = 'UPDATE pedido SET "idCliente"=:idCliente, fecha=:fecha, notas=:notas WHERE id=:id';
        $p = explode(";",$pedido->getCSV()); // obtenemos los campos en un array para usarlo de una manera más sencilla
        $parametros = array(
            ":id"=>$p[0],
            ":idCliente"=>$p[1],
            ":fecha"=>$p[2],
            ":notas"=>$p[3]);

        return $this->Update($consulta, $parametros);
    }

    function PedidoDelete($id){ // para borrar el pedido
        return $this->Delete("pedido", $id); // devolverá el resultado de la consulta DELETE FROM
    }

    // funciones líneas
    function LineaReadAll(){ // devuelve un vector de objetos líneas
        $filas = $this->ReadAll("linea"); // hace un SELECT * de la tabla indicada
        $array = [];

        foreach ($filas as $f) 
            $array[] = new Linea($f->id,$f->idPedido,$f->idProducto,$f->cantidad);
        
        return $array; // devolvemos el array de objetos encontrados
    }
    function LineaCreate($linea){ // para crear el objeto linea en el fichero
        $consulta = "INSERT INTO linea VALUES (:id, :idPedido, :idProducto, :cantidad)";
        $l = explode(";",$linea->getCSV()); // obtenemos los campos en un array para usarlo de una manera más sencilla
        $parametros = array(
            ":id"=>$l[0],
            ":idPedido"=>$l[1],
            ":idProducto"=>$l[2],
            ":cantidad"=>$l[3]);

        return $this->Create($consulta, $parametros);
    }

    function LineaRead($id){ // para leer el linea con ese id. Devuelve el objeto del fichero
        $f = $this->Read("linea", $id); // hace un SELECT de la tabla indicada
        // devolvemos el objeto encontrado o null
        return (!$f) ? null : new Linea($f->id,$f->idPedido,$f->idProducto,$f->cantidad);
    }

    function LineaUpdate($linea){ // para actualizar el objeto linea en un fichero
        $consulta = 'UPDATE linea SET "idPedido"=:idPedido, "idProducto"=:idProducto, cantidad=:cantidad WHERE id=:id';
        $l = explode(";",$linea->getCSV()); // obtenemos los campos en un array para usarlo de una manera más sencilla
        $parametros = array(
            ":id"=>$l[0],
            ":idPedido"=>$l[1],
            ":idProducto"=>$l[2],
            ":cantidad"=>$l[3]);

        return $this->Update($consulta, $parametros);
    }

    function LineaDelete($id){ // para borrar el linea
        return $this->Delete("linea", $id); // devolverá el resultado de la consulta DELETE FROM
    }
}

?>