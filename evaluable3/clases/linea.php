<?php
class Linea {
	// propiedades
    private $id;
    private $cantidad;
    private $idPedido;
    private $idProducto;

    public static $cont = 0; // será un contador global, para saber la última id

    // constructor
    public function __construct($id, $idPedido,$idProducto, $cantidad) {
        $this->id = $id;
        $this->idPedido = $idPedido;
        $this->idProducto = $idProducto;
        $this->cantidad = $cantidad;

        if ($id > self::$cont) // si la id global es menor que la nueva id
            self::$cont = $id; // actualizamos el valor de la id global
    }

    // métodos
	public function getId(){
		return $this->id;
	}

	public function setId($id){
		$this->id = $id;
	}

	public function getCantidad(){
		return $this->cantidad;
	}

	public function setCantidad($cantidad){
		$this->cantidad = $cantidad;
	}

	public function getIdPedido(){
		return $this->idPedido;
	}

	public function setIdPedido($idPedido){
		$this->idPedido = $idPedido;
	}

	public function getIdProducto(){
		return $this->idProducto;
	}

	public function setIdProducto($idProducto){
		$this->idProducto = $idProducto;
	}

	public function getCSV() {
		return $this->getID().";".$this->getIdPedido().";".";".$this->getIdProducto().";".$this->getCantidad();
	}

	public static function getUltimaID() {
		return self::$cont;
	}
}