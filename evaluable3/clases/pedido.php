<?php
class Pedido {
  	// propiedades
    private $id;
    private $idCliente;
    private $fecha;
    private $notas;

    public static $cont = 0; // será un contador global, para saber la última id

    // constructor
    public function __construct($id, $idCliente, $fecha, $notas) {
        $this->id = $id;
        $this->idCliente = $idCliente;
        $this->fecha = $fecha;
        $this->notas = $notas;

        if ($id > self::$cont) // si la id global es menor que la nueva id
            self::$cont = $id; // actualizamos el valor de la id global
    }

    // métodos
    public function getId(){
		return $this->id;
	}

	public function setId($id){
		$this->id = $id;
	}
	public function getIdCliente(){
		return $this->idCliente;
	}

	public function setIdCliente($idCliente){
		$this->idCliente = $idCliente;
	  }

	public function getFecha(){
        return $this->fecha;
	}

	public function setFecha($fecha){
		$this->fecha = $fecha;
	}

  public function getNotas(){
		return $this->notas;
	}

	public function setNotas($notas){
		$this->notas = $notas;
  }

  public function getCSV() {
		return $this->getID().";".$this->getIdCliente().";".$this->getFecha().";".$this->getNotas();
	}

	public static function getUltimaID() {
		return self::$cont;
	}
}