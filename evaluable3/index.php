<?php
    error_reporting(E_ALL); // mostramos todos los errores
    ini_set('display_errors', '1');

    include_once('util.php');

    session_start(); // iniciamos la sesión
    if (!isset($_SESSION["usuario"])) // si no hay usuario activo
        header("Location: login.php"); // cargamos la página de login

?>

<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <link rel="stylesheet" type="text/css" href="estilos.css">
    <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
</head>
<body>
    <div>
        <h1>Inicio</h1>
        <h2>Menú</h2>
    <ul class="menu">
        <li><a href="articulos.php">Artículos</a></li>
        <li><a href="clientes.php">Clientes</a></li>
        <li><a href="pedidos.php">Pedidos</a></li>
        <li><abbr title="Cerrar sesión"><a class="logout" href="logout.php"><i class="material-icons">exit_to_app</i></a></abbr></li>
    </ul>
    <h2>Información</h2>
    <ul class="menu">
        <li><a href="enunciado.html">Enunciado de la práctica</a></li>
        <li><a href="documentacion.html">Documentación del alumno</a></li>
        <li><a href="https://bitbucket.org/angeldefez/angeldefezdws">BitBucket alumno</a></li>
        <li><a href="https://angeldefezdws.herokuapp.com">Heroku alumno</a></li>
    </ul>
    <h2>Scripts</h2>
    <ul class="menu">
        <li><a href="postgres.html" target="_blank">Script Postgres</a></li>
        <li><a href="mysql.html" target="_blank">Script MySQL</a></li>
        <li><a href="script.php">Script creación BBDD Postgres en Heroku</a></li>
    </ul>
    <a class="derecha" href="../index.html">Volver</a>
    <?php footer() ?>
</div>
</body>
</html>