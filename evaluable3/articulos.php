<?php
    error_reporting(E_ALL); // mostramos todos los errores
    ini_set('display_errors', '1');

    include_once('util.php'); // incluímos el archivo más importante, que incluye todos los demás necesarios
    session_start(); // iniciamos la sesión
?>

<!DOCTYPE html>
<html>

<head>
    <link rel="stylesheet" type="text/css" href="estilos.css">
    <meta charset="utf-8">
</head>

<body>
    <div>
    <h1>Artículos</h1>
    <h2>Menú</h2>

    <?php
    $datos = null; // contendrá la fuente de datos, según elijamos
    compruebaSesion(); // validamos que las variables de sesión estén inicializadas y correctas

    $campos = array("ID","Nombre","Precio","Descripcion");

    // interceptamos la opción elegida y mostramos un menú u otro, según convenga

        mostrarMenu(); // mostramos el menú principal

    ?>
    <span class="limpia"></span>
    <?php footer() ?>
    </div>
    <?php
    // muestra el menú de cada página. al tratarse de objetos, cada uno tiene su propio menú
    function mostrarMenu() {
        global $datos, $op, $url_actual, $id, $campos;
        $objetos = $datos->ArticuloReadAll();

        switch ($op) {
            case "c":
                $nuevaID = Articulo::getUltimaID()+1;
                mostrarFormulario($nuevaID, $op);
                crearEnlace($url_actual,"Volver","derecha");
                break;
            case "r":
                crearEnlace($url_actual."?op=c","Crear nuevo","derecha");
                $o[]= $datos->ArticuloRead($id);
                muestraTabla($o, $campos, false);
                crearEnlace($url_actual,"Volver","derecha");
                break;
            case "u":
                mostrarFormulario($id, $op);
                crearEnlace($url_actual,"Volver","derecha");
                break;
            case "d":
                $datos->ArticuloDelete($id);
                $objetos = $datos->ArticuloReadAll(); // actualizamos los datos
                crearEnlace($url_actual."?op=c","Crear nuevo","derecha");
                if (!empty($objetos))
                    muestraTabla($objetos, $campos, true);
                break;
            case "e": // resultado de un formulario
                $array = validarDatos($campos);
                procesaResultado($array);
                    break;
            default:
                crearEnlace($url_actual."?op=c","Crear nuevo","derecha");
                if (!empty($objetos))
                    muestraTabla($objetos, $campos, true);
        }
        if (!empty($objetos)) // si hay objetos que mostrar
            crearEnlace("index.php","Inicio","derecha");
        elseif ($op != "c") // si no viene de crear un objeto
            mensajeError("No hay artículos que mostrar");

    }

    // muestra el formulario, cada tabla tiene el suyo propio
    function mostrarFormulario($id, $op) {
        global $datos;
        $objeto = $datos->ArticuloRead($id);
        $nombre = ($objeto) ? $objeto->getNombre() : null; // si existe el objeto, llenamos los campos
        $precio = ($objeto) ? $objeto->getPrecio() : null;
        $descripcion = ($objeto) ? $objeto->getDescripcion() : null;
        $titulo = ($op == 'c') ? "Crear Nuevo":"Modificar";
    
        echo '<form name="formulario" method="post" action="articulos.php?op=e&id='.$id.'">';
        echo '<fieldset>';
        echo '<legend>'.$titulo.' Artículo</legend>';
        echo '<p>ID: <input type="text" name="ID" size="5" required readonly value='.$id.'></p>'; // sólo lectura, no se puede modificar
        echo '<p>Nombre: <input type="text" name="Nombre" size="80" required value="'.$nombre.'"></p>';
        echo '<p>Precio: <input type="text" name="Precio" size="6" required value="'.$precio.'"></p>';
        echo 'Descripción: <p><textarea name="Descripcion" rows="5" cols="50">'.$descripcion.'</textarea></p>'; // no sé por qué pero se bloquea en Firefox
        echo '<input name="o" type="hidden" value="'.$op.'">';
        echo '<input type="submit" value="Enviar">';
        echo '<input type="reset" value="Reiniciar">';
        echo '</fieldset>';
        echo '</form>';
    }

    function crearNuevo($tabla) {
        global $datos;
        return $datos->ArticuloCreate(new Articulo($tabla["ID"],$tabla["Nombre"],$tabla["Precio"],$tabla["Descripcion"]));
    }

    function actualizarTabla($tabla) {
        global $datos;
        return $datos->ArticuloUpdate(new Articulo($tabla["ID"],$tabla["Nombre"],$tabla["Precio"],$tabla["Descripcion"]));
    }
    ?>
</body>
</html>