DROP TABLE IF EXISTS linea;
DROP TABLE IF EXISTS pedido;
DROP TABLE IF EXISTS articulo;
DROP TABLE IF EXISTS cliente;

CREATE TABLE "cliente" (
  "id" SERIAL PRIMARY KEY,
  "nombre" varchar(50) NOT NULL,
  "email" varchar(50) NOT NULL,
  "usuario" varchar(20) NOT NULL,
  "password" varchar(20) NOT NULL,
  "dni" varchar(9) NOT NULL,
  "foto" varchar(50)
);

CREATE TABLE "articulo" (
  "id" SERIAL PRIMARY KEY,
  "nombre" varchar(100) NOT NULL,
  "precio" decimal(6,2) NOT NULL,
  "descripcion" text
);

CREATE TABLE "pedido" (
  "id" SERIAL PRIMARY KEY,
  "idCliente" int,
  "fecha" varchar(10) NOT NULL,
  "notas" varchar(255)
);

CREATE TABLE "linea" (
  "id" SERIAL,
  "idPedido" int,
  "idProducto" int,
  "cantidad" int NOT NULL,
  PRIMARY KEY ("id", "idPedido")
);

ALTER TABLE "pedido" ADD FOREIGN KEY ("idCliente") REFERENCES "cliente" ("id") ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE "linea" ADD FOREIGN KEY ("idPedido") REFERENCES "pedido" ("id") ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE "linea" ADD FOREIGN KEY ("idProducto") REFERENCES "articulo" ("id") ON DELETE CASCADE ON UPDATE CASCADE;

INSERT INTO cliente VALUES (1,'Ángel de Fez','angeldefez@email.com','angeldefez',1234,'12345678X','angeldefez.jpg');
INSERT INTO cliente VALUES (2,'Paco Aldarias','pacoaldarias@ceedcv.com','pacoaldarias',1234,'12345678A','pacoaldarias.png');
INSERT INTO cliente VALUES (3,'Joaquín Gilabert','joaquingilabert@ceedcv.com','joaquingilabert',1234,'12345679B','joaquin.jpg');
INSERT INTO cliente VALUES (4,'Sandra Palmo','sandrapalmo@ceedcv.com','sandrapalmo',1234,'12345680C','sandra.jpg');
INSERT INTO cliente VALUES (5,'Irene Espinar','irene3spinar@ceedcv.com','irene3spinar',1234,'12345681D','irene.jpg');
INSERT INTO cliente VALUES (6,'Pablo Martín','pablomartin@ceedcv.com','pablomartin',1234,'12345682E','pablo.jpg');

INSERT INTO articulo VALUES (1,'Kingston A400 SSD 240GB',32.99,'Se trata del modelo más modesto de los que Kingston tiene actualmente en el mercado y se dirige a usuarios de presupuesto limitado que busquen sustituir su disco duro mecánico para incrementar el rendimiento de su equipo.');
INSERT INTO articulo VALUES (2,'Xiaomi Mi Air 13.3',899,'El Xiaomi Mi Notebook Aires ligero, es compacto, y podrás llevarlo fácilmente a cualquier lugar sin darte cuenta de que lo llevas encima.');
INSERT INTO articulo VALUES (3,'Huawei P20 Lite 64GB Azul Libre',233.24,'Huawei P20 Lite parece estar destinado a ser un superventas, con una relación calidad/precio sorprendente.');
INSERT INTO articulo VALUES (4,'LG 43UK6200PLA 43" LED IPS UltraHD 4K',299,'La mejor pantalla calidad precio de toda la página, es 4K nativo.');
INSERT INTO articulo VALUES (5,'Logitech B100 Ratón Negro',4.99,'Cómodo, diseño ambidiestro La forma cómoda y de bajo perfil se siente bien en cualquier mano, incluso después de un largo día de trabajo.');
INSERT INTO articulo VALUES (7,'Samsung UE50NU7025 50" LED UltraHD 4K',385,'Vive una experiencia visual a todo color. Por fin podrás disfrutar de tu contenido preferido con colores más vivos y realistas.');
INSERT INTO articulo VALUES (8,'Kobo Clara HD eReader 6"',129,'El Kobo Clara HD es el compañero perfecto para cualquier amante de la lectura. Siempre proporciona la mejor luz de lectura, gracias a la ComfortLight PRO, y es garantía de una experiencia de lectura natural, como si se tratase de papel impreso, con su excepcional pantalla HD de 6".');
INSERT INTO articulo VALUES (9,'Canon Pixma MX475 Multifunción WiFi/Fax',52.35,'Equipo Todo en Uno para oficina doméstica con conectividad inalámbrica y alimentador automático de documentos Imprime, copia, escanea y envía fax con facilidad con este asequible equipo Todo en Uno Wi-Fi.');
INSERT INTO articulo VALUES (10,'Grand Theft Auto V PS4',28.63,'Vive la historia de Trevor, Michael y Franklin a través de Los Santos y el condado de Blaine. Juega la campaña principal o piérdete entre sus calles con las innumerables tareas y actividades opcionales a las que podrás acceder.');

INSERT INTO pedido VALUES (1,1,'22/10/2019','Preferiblemente por la mañana.');
INSERT INTO pedido VALUES (2,3,'23/10/2019','Dejar el paquete en la portería.');

INSERT INTO linea VALUES (1,1,10,1);
INSERT INTO linea VALUES (2,1,8,1);
INSERT INTO linea VALUES (3,1,5,1);
INSERT INTO linea VALUES (4,2,1,2);
