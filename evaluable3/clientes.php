<?php
    error_reporting(E_ALL); // mostramos todos los errores
    ini_set('display_errors', '1');

    include_once('util.php'); // incluímos el archivo más importante, que incluye todos los demás necesarios
    session_start(); // iniciamos la sesión
?>

<!DOCTYPE html>
<html>

<head>
    <link rel="stylesheet" type="text/css" href="estilos.css">
    <meta charset="utf-8">
</head>

<body>
    <div>
    <h1>Clientes</h1>
    <h2>Menú</h2>

    <?php
    $datos = null; // contendrá la fuente de datos, según elijamos
    compruebaSesion(); // validamos que las variables de sesión estén inicializadas y correctas

    $campos = array("ID","Nombre","Email","Usuario","Password","DNI","Foto"); // los campos que contiene tanto el formulario como la tabla
    mostrarMenu(); // mostramos el menú principal

    ?>
    <span class="limpia"></span>
    <?php footer() ?>
    </div>
    <?php
    // muestra el menú de cada página. al tratarse de objetos, cada uno tiene su propio menú
    function mostrarMenu() {
        global $datos, $op, $url_actual, $id, $campos;
        $objetos = $datos->ClienteReadAll();
        switch ($op) {
            case "c":
                $nuevaID = Cliente::getUltimaID()+1;
                mostrarFormulario($nuevaID, $op);
                crearEnlace($url_actual,"Volver","derecha");
                break;
            case "r":
                echo '<img class="foto" src="imagenes/'.$datos->ClienteRead($id)->getFoto().'" alt="Foto de cliente" height="100" width="100">';
                crearEnlace($url_actual."?op=c","Crear nuevo","derecha");
                $o[]= $datos->ClienteRead($id);
                muestraTabla($o, $campos, false);
                crearEnlace($url_actual,"Volver","derecha");
                break;
            case "u":
                mostrarFormulario($id, $op);
                crearEnlace($url_actual,"Volver","derecha");
                break;
            case "d":
                $datos->ClienteDelete($id);
                $objetos = $datos->ClienteReadAll(); // actualizamos los datos
                crearEnlace($url_actual."?op=c","Crear nuevo","derecha");
                if (!empty($objetos))
                    muestraTabla($objetos, $campos, true);
                break;
            case "e": // resultado de un formulario
                    $array = validarDatos($campos); // validamos los campos del formulario
                    // hay campos que es mejor validarlos a mano
                    $array["Password"] = (limpiaCampo("password1") == limpiaCampo("password2") ? limpiaCampo("password1") : null);
                    $array["Foto"] = obtenerFoto("Foto","imagenes/");
                    procesaResultado($array);
                    break;
            default:
                crearEnlace($url_actual."?op=c","Crear nuevo","derecha");
                if (!empty($objetos))
                    muestraTabla($objetos, $campos, true);
        }
        if (!empty($objetos)) // si hay objetos que mostrar
            crearEnlace("index.php","Inicio","derecha");
        elseif ($op != "c") // si no viene de crear un objeto
            mensajeError("No hay artículos que mostrar");
    }
        // muestra el formulario, cada tabla tiene el suyo propio
        function mostrarFormulario($id, $op) {
            global $datos;
            $objeto = $datos->ClienteRead($id);
            $nombre = ($objeto) ? $objeto->getNombre() : null; // si existe el objeto, llenamos los campos
            $email = ($objeto) ? $objeto->getEmail() : null;
            $password = ($objeto) ? $objeto->getPassword() : null;
            $user = ($objeto) ? $objeto->getUsuario() : null;
            $dni = ($objeto) ? $objeto->getDNI() : null;
            $foto = ($objeto) ? $objeto->getFoto() : null;

            $titulo = ($op == 'c') ? "Crear Nuevo":"Modificar";
        
            echo '<form name="formulario" method="post" action="clientes.php?op=e&id='.$id.'" enctype="multipart/form-data">';
            echo '<fieldset>';
            echo '<legend>'.$titulo.' Cliente</legend>';
            echo '<img class="form" src="imagenes/'.$foto.'" alt="Foto de cliente" height="100" width="100">';
            echo '<p>ID: <input type="text" name="ID" size="5" required readonly value='.$id.'></p>'; // sólo lectura, no se puede modificar
            echo '<p>Nombre y apellidos: <input type="text" name="Nombre" size="50" required value="'.$nombre.'"></p>';
            echo '<p>E-mail: <input type="text" name="Email" size="30" required value="'.$email.'"></p>';
            echo '<p>Nombre de usuario: <input type="text" name="Usuario" size="20" required value="'.$user.'"></p>';
            echo '<p>DNI: <input type="text" name="DNI" size="20" required value="'.$dni.'"></p>';
            echo '<p>Contraseña: <input type="password" name="password1" size="20" required value="'.$password.'"> Introduce de 7 a 20 caráceres</p>';
            echo '<p>Repite contraseña: <input type="password" name="password2" size="20" required value="'.$password.'"></p>';
            echo '<p>Foto: <input type="file" name="Foto" value="'.$foto.'"/></p>';
            echo '<input type="hidden" name="max_file_size" value="102400" >';
            echo '<input name="o" type="hidden" value="'.$op.'">';
            echo '<input type="submit" value="Enviar">';
            echo '<input type="reset" value="Reiniciar">';
            echo '</fieldset>';
            echo '</form>';
        }

        function crearNuevo($tabla) {
            global $datos;
            return $datos->ClienteCreate(new Cliente($tabla["ID"],$tabla["Nombre"],$tabla["Email"],$tabla["Usuario"],$tabla["Password"],$tabla["DNI"],$tabla["Foto"]));
        }
    
        function actualizarTabla($tabla) {
            global $datos;
            return $datos->ClienteUpdate(new Cliente($tabla["ID"],$tabla["Nombre"],$tabla["Email"],$tabla["Usuario"],$tabla["Password"],$tabla["DNI"],$tabla["Foto"]));
        }
    ?>
</body>
</html>