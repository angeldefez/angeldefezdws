<?php
    error_reporting(E_ALL); // mostramos todos los errores
    ini_set('display_errors', '1');

    include_once('util.php');

    session_start(); // iniciamos la sesión
    $usuario = limpiaCampo("usuario"); // recogemos el usuario enviado por el formulario, si es que lo hay

    if (isset($_SESSION["usuario"]))
        header("Location: index.php"); // cargamos la página de inicio
    else if (empty($usuario)) // si no hay usuario activo
        mostrarLogin(); // mostramos la pantalla de login
    else
        comprobarDatos(); // comprobamos el usuario y el password entregados, según la fuente de datos elegida
?>

<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <link rel="stylesheet" type="text/css" href="estilos.css">
    <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
</head>
<body>
<?php
    // pantalla de login si no hay usuario activo
    function mostrarLogin() {
        echo '<div class="login"><form action="login.php" method="post">';
        echo '<abbr title="Nombre de usuario"><i class="material-icons">person</i></abbr> <input name="usuario" type="text" placeholder="Nombre de usuario" value="pacoaldarias">';
        echo '<br><br>';
        echo '<abbr title="Contraseña"><i class="material-icons">lock</i></abbr> <input name="password" type="password" placeholder="Contraseña" value="1234">';
        echo '<br><br>';
        echo '<abbr title="Fuente de datos"><i class="material-icons">sd_storage</i></abbr> <select name="fuente">';
        echo '<option value="postgres">Postgres remoto</option>'; 
        echo '<option value="csv">CSV local</option></select>';
        echo '<br><br>';
        echo '<input type="submit" value="Iniciar sesión"></form>';
        echo '<br><br>';
        echo '<abbr title="Usuario: pacoaldarias Contraseña: 1234">Ayuda</abbr>';
        echo '</div>';
    }

    function comprobarDatos() {

        $fuente = limpiaCampo("fuente");
        $usuario = limpiaCampo("usuario");
        $password = limpiaCampo("password");

        if ($fuente == "postgres") 
            $datos = new Postgres();
        else {
            $datos = new Fichero();
            $datos->Save("datos/clientes.csv", $datos->ReadAll("datos/clientes.txt")); // creamos el archivo de clientes
        }

        $usuarios = $datos->ClienteReadAll();
            
        foreach($usuarios as $u) {
            if ($u->getUsuario() == $usuario) { // el cuarto campo es el nombre de usuario
                $pass = $u->getPassword(); // el quinto campo es el password
                $id = $u->getId();
                break;
            }
        }

        if ($pass == $password) { // el login es correcto

            if ($fuente == "csv") {
            // copiamos la base de datos base a los archivos CSV
                $datos->Save("datos/clientes.csv", $datos->ReadAll("datos/clientes.txt")); // creamos el archivo de clientes
                $datos->Save("datos/articulos.csv", $datos->ReadAll("datos/articulos.txt")); // creamos el archivo de articulos
                $datos->Save("datos/pedidos.csv", $datos->ReadAll("datos/pedidos.txt")); // creamos el archivo de pedidos
                $datos->Save("datos/lineas.csv", $datos->ReadAll("datos/lineas.txt")); // creamos el archivo de líneas de pedidos
            }

            $_SESSION["usuario"] = $id; // añadimos la ID del usuario logueado a la variable de sesión "usuario"
            $_SESSION["datos"] = $fuente; // añadimos la fuente de datos elegida a la variable de sesión "datos"
            header("Location: index.php"); // cargamos la página de inicio
        } else {
            mensajeError("Error, usuario o contraseña incorrectos");
        }
    }
?>
</body>
</html>