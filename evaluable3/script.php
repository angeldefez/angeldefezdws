<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <link rel="stylesheet" type="text/css" href="estilos.css">
    <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
</head>
<body>
    <div>
        <h1>Reinicio de la BBDD</h1>
        <h2>Menú</h2>
        <a class="derecha" href="index.php">Volver</a>
        <div>
    
        <?php
            include_once("util.php");

            if(isset($_REQUEST["borrar"]))
                borrarBD();
            else
                mensajeError("El siguiente script reiniciará la BBDD de Postgres en Heroku. ¿Deseas continuar?", "script.php?borrar", "OK");
        ?>

        </div>
    
    <?php footer() ?>
</div>
</body>
</html>

<?php
function borrarBD() {
include_once("clases/config.php");

$conexion = new PDO(
    'pgsql:host='.Config::$bdhostname.
    ';dbname='.Config::$bdnombre,
    // ';charset=utf8',
    Config::$bdusuario,
    Config::$bdclave);

$handle = fopen("postgres.sql", "r");
if ($handle) {
    echo "<pre><code class='language-mysql' lang='mysql'>";
    $consulta = "";
    while (($linea = fgets($handle)) !== false) {
        $consulta .= $linea;
        if (trim(substr($linea,-2)) == ";") {
            consulta($conexion, $consulta);
            $consulta = "";
        }
    }
    fclose($handle);
} else
    echo "Algo ha salido mal";

    $conexion = null;
    echo "</code></pre>";

    crearEnlace("index.php","Volver");
}

function consulta($conexion, $consulta) {
    $result = $conexion->query($consulta);
    if (!$result)
        echo "<b>Error</b> -> ";
    else
        echo "<b> OK -> </b>";
    echo ltrim($consulta)."<br>";
}


?>