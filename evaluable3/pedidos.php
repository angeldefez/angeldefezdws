<?php
    error_reporting(E_ALL); // mostramos todos los errores
    ini_set('display_errors', '1');

    include_once('util.php'); // incluímos el archivo más importante, que incluye todos los demás necesarios
    session_start(); // iniciamos la sesión

    $articulos = isset($_REQUEST["articulos"]) ? $_REQUEST["articulos"] : null; // recogemos la lista de artículos del pedido

?>

<!DOCTYPE html>
<html>

<head>
    <link rel="stylesheet" type="text/css" href="estilos.css">
    <meta charset="utf-8">
</head>

<body>
    <div>
    <h1>Pedidos</h1>
    <h2>Menú</h2>
    <?php
    $datos = null; // contendrá la fuente de datos, según elijamos
    compruebaSesion(); // validamos que las variables de sesión estén inicializadas y correctas

    $campos = array("ID","idCliente","Fecha","Notas"); // los campos que contiene tanto el formulario como la tabla
    mostrarMenu(); // mostramos el menú principal
//     $db = new PDO(
//         'pgsql:host='.Config::$bdhostname.
//         ';dbname='.Config::$bdnombre,
//         // ';charset=utf8',
//         Config::$bdusuario,
//         Config::$bdclave);

//         $c = "INSERT INTO linea VALUES (5, 1, 1, 13)";
//         // INSERT INTO linea VALUES (4,2,1,2);
//         // $c = "SELECT * FROM linea";
// //     print_r($datos->lineaCreate(new Linea(null,1,2,3)));    
//     $result = $db->prepare($c); // preparamos la consulta
//     $result->execute();
//     $o = $result->fetchAll(PDO::FETCH_OBJ);
// //     print_r($db->errorInfo());
// //     print_r($o);
   
    ?>
    <span class="limpia"></span>
    <?php footer() ?>
    </div>
    <?php
    // muestra el menú de cada página. al tratarse de objetos, cada uno tiene su propio menú
    function mostrarMenu() {
        global $datos, $op, $url_actual, $id, $campos;
        $objetos = $datos->PedidoReadAll();
        switch ($op) {
            case "c":
                $nuevaID = Pedido::getUltimaID()+1;
                mostrarFormulario($nuevaID, $op);
                crearEnlace($url_actual,"Volver","derecha");
                break;
            case "r":
                crearEnlace($url_actual."?op=c","Crear nuevo","derecha");
                $o[] = $datos->PedidoRead($id);
                muestraTabla($o, $campos, false);
                muestraLineas($id);
                crearEnlace($url_actual,"Volver","derecha");
                break;
            case "u":
                mostrarFormulario($id, $op);
                crearEnlace($url_actual,"Volver","derecha");
                break;
            case "d":
                $datos->PedidoDelete($id);
                $objetos = $datos->PedidoReadAll(); // actualizamos los datos
                crearEnlace($url_actual."?op=c","Crear nuevo","derecha");
                if (!empty($objetos))
                    muestraTabla($objetos, $campos, true);
                break;
            case "e": // resultado de un formulario
                $array = validarDatos($campos); // validamos los campos del formulario
                // hay campos que es mejor validarlos a mano
                $e = procesaResultado($array);
                // if ($e)
                    lineaPedidos($id); // si ha salido bien, actualizamos las lineas del pedido
                break;
            default:
                crearEnlace($url_actual."?op=c","Crear nuevo","derecha");
                if (!empty($objetos))
                    muestraTabla($objetos, $campos, true);
        }
        if (!empty($objetos)) // si hay objetos que mostrar
            crearEnlace("index.php","Inicio","derecha");
        elseif ($op != "c") // si no viene de crear un objeto
            mensajeError("No hay artículos que mostrar");

    }

    // muestra el formulario, cada tabla tiene el suyo propio
        function mostrarFormulario($id, $op) {
            global $datos;
            $objeto = $datos->PedidoRead($id);
            $fecha = ($objeto) ? $objeto->getFecha() : null; // si existe el objeto, llenamos los campos
            $idCliente = ($objeto) ? $objeto->getIDCliente() : null;
            $notas = ($objeto) ? $objeto->getNotas() : null;
            
            $titulo = ($op == 'c') ? "Crear Nuevo":"Modificar";
        
            echo '<form name="formulario" method="post" action="pedidos.php?op=e&id='.$id.'">';
            echo '<fieldset>';
            echo '<legend>'.$titulo.' Pedido</legend>';
            echo '<p>ID: <input type="text" name="ID" size="5" required readonly value='.$id.'></p>'; // sólo lectura, no se puede modificar
            echo '<p>Fecha: <input type="text" name="Fecha" size="10" required value="'.$fecha.'"></p>';
            echo '<p>Cliente: <select name="idCliente">';
            $clientes = $datos->ClienteReadAll();
            foreach ($clientes as $c)
                echo '<option value="'.$c->getId().'" '.(($c->getId() == $idCliente) ? 'selected':'').'> '.$c->getId()." - ".$c->getNombre().'</option>'; 

            echo '</select></p>';
            echo 'Notas: <br><textarea name="Notas" rows="5" cols="80">'.$notas.'</textarea><br>'; // no sé por qué pero se bloquea en Firefox
            muestraArticulos($id, $op);
            echo '<br><input type="submit" value="Enviar">';
            echo '<input type="reset" value="Reiniciar">';
            echo '<input name="o" type="hidden" value="'.$op.'">';
            echo '</fieldset>';
            echo '</form>';
        }

        function crearNuevo($tabla) {
            global $datos;
            return $datos->PedidoCreate(new Pedido($tabla["ID"],$tabla["idCliente"],$tabla["Fecha"],$tabla["Notas"]));
        }
    
        function actualizarTabla($tabla) {
            global $datos;
            return $datos->PedidoUpdate(new Pedido($tabla["ID"],$tabla["idCliente"],$tabla["Fecha"],$tabla["Notas"]));
        }

        function muestraLineas($id) {
            global $datos;
            echo "<h3>Detalle del pedido</h3>";
            $pedidos = listaLineas($id);

            $detalles = []; // nombre, precio, unidades y total del artículo
            $total = 0; // sumatorio del pedido

            foreach($pedidos as $p) {
                $n = $p->getCantidad();
                $a = $datos->ArticuloRead($p->getIdProducto());
                $total += $n*$a->getPrecio();
                
                $detalles [] = array("Articulo" => $a->getNombre(), "Cantidad" => $n,
                "Precio" => $a->getPrecio()."€", "Total" => $n*$a->getPrecio()."€");
            }
            
            echo "<table>";
            arrayAtabla($detalles); // mostramos los detalles del pedido en una tabla
            echo "<tr><th class='derecha' colspan='3'>TOTAL PEDIDO</th><th>$total"."€</th></tr>";
            echo "</table>";
        }

        function muestraArticulos($id, $op) {
            global $datos;

            $ped = listaPedidos($id); // devuelve las líneas que corresponden a ese pedido
            $art = $datos->ArticuloReadAll(); // leemos todos los artículos

            $array = array();
            $total = 0; // sumatorio del pedido
            foreach($art as $a) {
                $c = isset($ped[$a->getId()]) ? $ped[$a->getId()] : null;

                    $total += $c*$a->getPrecio();
                    $input = "<input type='text' name='articulos[".$a->getId()."]' size='5' pattern='[0-9]{1,5}|null' value='".$c."'>";
                    if ($op != "u" xor $c) // sólo muestra los que están vacíos cuando no es una opción de update
                        $array [] = array("Artículo" => $a->getNombre(), "Cantidad" => $input,
                        "Precio" => $a->getPrecio()."€", "Total" => $a->getPrecio()*$c."€");
            }
            
            echo "<table>";
            arrayAtabla($array); // mostramos los detalles del pedido en una tabla
            echo "<tr><th class='derecha' colspan='3'>TOTAL PEDIDO</th><th>$total"."€</th></tr>";
            echo "</table>";
        }

        function lineaPedidos($id) {
            global $datos, $articulos;
            $ped = listaPedidos($id); // lista de líneas con la ID del pedido $id=>$cantidad

            // LA LISTA ESTÁ VACÍA CUANDO ES NUEVA ID!!!

            foreach($ped as $p=>$c) {
                $c = isset($articulos[$p]) ? $ped[$p] : null;
                if ($c)
                    $datos->LineaUpdate(new Linea(Linea::getUltimaID(),$id,$p,$c));
                else
                    $datos->LineaCreate(new Linea(Linea::getUltimaID()+1,$id,$p,$c));
            }

        }

        function listaPedidos($id) {
            global $datos;

            $lista = $datos->LineaReadAll();
            $ped = $datos->PedidoRead($id); // el objeto de Pedido que estamos viendo
            $pedidos = []; // aquí almacenaremos todas las líneas que sean del pedido
            if (!empty($ped)) {
                foreach ($lista as $l) {
                    if ($l->getIdPedido() == $ped->getId())
                        $pedidos[$l->getIdProducto()] = $l->getCantidad(); // añadimos a la lista el pedido indicado si coinciden idPedido e idCliente
                }
            }

            return $pedidos;
        }

        function listaLineas($id) {
            global $datos;
            $lineas = $datos->LineaReadAll(); // cargamos todos los datos de líneas que tenemos            

            $pedidos = []; // aquí almacenaremos todas las líneas que sean del pedido
            foreach ($lineas as $l) {
                if ($l->getIdPedido() == $id)
                    $pedidos[] = $l; // añadimos a la lista el pedido indicado si coinciden idPedido
            }
            return $pedidos;
        }
    ?>

</body>
</html>
