<?php

require_once __DIR__ . '/controller_articulos.php';
require_once __DIR__ . '/controller_mashup.php';

class Controller {
    public function inicio() {

        $params = array(
            'mensaje'=>'Ángel de Fez. DWES. Curso 2019/20',
            'fecha'=>"16-02-2020"
        );

        require __DIR__ . '/templates/inicio.php';
    }

    public function salir() {
        header('location: ../../index.html');
        exit();
    }
}

?>