<?php ob_start() ?>

<div id="menu">
<h1>Artículos</h1>
<ul class="menu">
    <li><a href="index.php?ctl=listar">Listar</a></li>
    <li><a href="index.php?ctl=crear">Nuevo</a></li>
    <li><a href="index.php?ctl=buscar">Buscar</a></li>
    <li><a href="index.php?ctl=ver_json">Ver JSON</a></li>
    <li><a href="index.php?ctl=ver_rss">ver RSS</a></li>
    <li><a href="index.php?ctl=ver_csv">ver CSV</a></li>
</ul>
</div>
<hr>

<?php
    if (!empty($params['extra']))
        echo $params['extra']; // mostramos el HTML generado en alguna de otras subpáginas
    
    $contenido = ob_get_clean()
?>

<?php include 'layout.php' ?>