<?php ob_start() ?>

<?php
    include_once('../app/articulo.php');
    error_reporting(E_ALL);
    ini_set('display_errors', '1');

    $op = (isset($_GET["ctl"])) ? $_GET["ctl"] : "";
    $tipo = (isset($_GET["tipo"])) ? $_GET["ctl"] : "BBDD";


?>

<h2>Lista de Artículos (<?php echo $tipo ?>)</h2>
<table><tbody>
    <tr>
        <th>ID</th>
        <th>Nombre</th>
        <th>Precio</th>
        <th>Descripción</th>
        <?php if ($op != "ver") : ?>
        <th colspan="3">Acciones</th>
        <?php endif; ?>
    </tr>

    <?php //print_r($params['articulos']) ?>
    <?php foreach ($params['articulos'] as $articulo) : ?>
        <tr>
            <td><?php echo $articulo->getId() ?></td>
            <td><?php echo $articulo->getNombre() ?></td>
            <td><?php echo $articulo->getPrecio() ?></td>
            <td><?php echo $articulo->getDescripcion() ?></td>
            <?php if ($op != "ver") :?>
            <td><a href="index.php?ctl=ver&id=<?php echo $articulo->getId() ?>">Ver</a>
            <a href="index.php?ctl=modificar&id=<?php echo $articulo->getId() ?>">Modificar</a>
            <a href="index.php?ctl=borrar&id=<?php echo $articulo->getId() ?>">Borrar</a></td>
            <?php endif; ?>
        </tr>
    <?php endforeach; ?>
</tbody></table>

<a class="enlace" href="index.php?ctl=json"><img src="./img/json.png" alt="json"></a>
<a class="enlace" href="index.php?ctl=rss"><img src="./img/rss.png" alt="rss"></a>
<a class="enlace" href="index.php?ctl=csv"><img src="./img/csv.png" alt="csv"></a>

<?php $params['extra'] = ob_get_clean() ?>

<?php include 'articulos.php' ?>