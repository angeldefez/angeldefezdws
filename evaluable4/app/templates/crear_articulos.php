<?php ob_start() ?>

<?php
include_once('../app/articulo.php');
error_reporting(E_ALL);
ini_set('display_errors', '1');
?>

<form name="formulario" method="post" action="index.php?ctl=<?php echo $params['accion'] ?>">
    <fieldset>
        <legend>Crear Artículo</legend>
        <p>ID: <input type="text" name="id" size="5" required readonly value="<?php echo $params['id'] ?>"></p>
        <p>Nombre: <input type="text" name="nombre" size="80" required value="<?php echo $params['nombre'] ?>"></p>
        <p>Precio: <input type="text" name="precio" size="6" required value="<?php echo $params['precio'] ?>"></p>
        Descripción: <p><textarea name="descripcion" rows="5" cols="50"><?php echo $params['descripcion'] ?></textarea></p>
        <input name="o" type="hidden" value="'.$op.'">
        <input type="submit" value="Enviar">
        <input type="reset" value="Reiniciar">
    </fieldset>
</form>


<?php $params['extra'] = ob_get_clean() ?>

<?php include 'articulos.php' ?>