<?php ob_start() ?>

<div id="menu">
<h1>Estaciones de Valenbisi</h1>
<form name="formulario" method="post" action="index.php?ctl=mashup">
    <fieldset>
        <legend>Selecciona una estación</legend>
        <select name="estacion">
            <?php foreach ($params["estaciones"] as $e) :?>
                <option value="<?php echo $e->id ?>"><?php echo $e->id." - ".$e->nombre ?></option>
            <?php endforeach ?>
        </select>
        <input type="submit" value="Seleccionar">
    </fieldset>
    </form>
    <div class = "estaciones">
    <?php
        if (isset($params["estacion"])) {
            echo "<h3>".$params["estacion"]->nombre."</h3>";
            echo "<p><strong>Bornetas: </strong>";
            echo "Disponibles: <span>".$params["estacion"]->disponibles;
            echo "</span> Libres: <span>".$params["estacion"]->libres;
            echo "</span> Total: <span>".$params["estacion"]->total;
            echo "</span></p><p>";
            echo "<strong>Coordenadas: </strong>".$params["estacion"]->coordenadas[0];
            echo ", ".$params["estacion"]->coordenadas[1];
            echo "</p>";
        }
    ?>
    </div>
</div>
<hr>

<?php
    $contenido = ob_get_clean();
?>

<?php include 'layout.php' ?>