<?php ob_start() ?>

<?php
include_once('../app/articulo.php');
error_reporting(E_ALL);
ini_set('display_errors', '1');
?>

<form name="formBusqueda" action="index.php?ctl=buscar" method="POST">
    <table>
        <tr>
            <td>Nombre de artículo:</td>
            <td><input type="text" name="nombre" size="35" value="<?php echo $params['nombre'] ?>"></td>
            <td>(puedes utilizar '%' como comodín)</td>
            <td><input type="submit" value="buscar"></td>
        </tr>
    </table>
    </table>
</form>
<?php if (count($params['articulos']) > 0) : ?>
    <h2>Lista de Artículos</h2>
<table><tbody>
    <tr>
        <th>ID</th>
        <th>Nombre</th>
        <th>Precio</th>
        <th>Descripción</th>
        <th colspan="3">Acciones</th>
    </tr>

    <?php //print_r($params['articulos']) ?>
    <?php foreach ($params['articulos'] as $articulo) : ?>
        <tr>
            <td><?php echo $articulo->getId() ?></td>
            <td><?php echo $articulo->getNombre() ?></td>
            <td><?php echo $articulo->getPrecio() ?></td>
            <td><?php echo $articulo->getDescripcion() ?></td>
            <td><a href="index.php?ctl=ver&id=<?php echo $articulo->getId() ?>">Ver</a>
            <a href="index.php?ctl=modificar&id=<?php echo $articulo->getId() ?>">Modificar</a>
            <a href="index.php?ctl=borrar&id=<?php echo $articulo->getId() ?>">Borrar</a></td>
        </tr>
    <?php endforeach; ?>
</tbody></table>
<?php endif; ?>

<?php $params['extra'] = ob_get_clean() ?>

<?php include 'articulos.php' ?>