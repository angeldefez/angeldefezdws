<?php ob_start() ?>

<h1>Inicio</h1>
<h3>Fecha: <?php echo $params['fecha'] ?></h3>
<p><?php echo $params['mensaje'] ?></p>
<ul class="menu">
    <li><a href="enunciado.html">Evaluable4 Enunciado</a></li>
    <li><a href="documentacion.html">Documentación del alumno</a></li>
</ul>

<?php $contenido = ob_get_clean() ?>

<?php include 'layout.php' ?>