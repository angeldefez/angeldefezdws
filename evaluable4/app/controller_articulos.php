<?php

class ControllerArticulos {

    // artículos

    public function articulos() {
        require __DIR__ . '/templates/articulos.php';
    }

    public function listar() {
        $m = new Model(); // creamos el Modelo
        $params = array('articulos'=>$m->listaArticulos(true)); // pasamos por parámetro la lista de artículos

        require __DIR__ . '/templates/mostrar_articulos.php';
    }

    public function buscar() {
        $params = array(
            'nombre'=>'',
            'articulos'=>array()
        );

        $m = new Model(); // creamos el Modelo

        if ($_SERVER['REQUEST_METHOD'] == 'POST') { // si hemos enviado algo ya, ponemos los datos en el formulario
            $params['nombre'] = $_POST['nombre'];
            $params['articulos'] = $m->buscarArticulos($_POST['nombre']);
        }

        require __DIR__ . '/templates/buscar_articulos.php';
    }

    public function crear() {
        $m = new Model(); // creamos el Modelo
        $result = null; // nos indicará si la operación ha sido correcta

        $params = array( // definimos los parámetros a pasar
            'id'=>$m->ultimaID(),
            'nombre'=>'',
            'precio'=>'',
            'descripcion'=>'',
            'accion'=>'crear'
        );

        if ($_SERVER['REQUEST_METHOD'] == 'POST') { // si ha habido envío, llenamos los datos del formulario
            $params['id'] = $this->limpiaCampo('id');
            $params['nombre'] = $this->limpiaCampo('nombre');
            $params['precio'] = $this->limpiaCampo('precio');
            $params['descripcion'] = $this->limpiaCampo('descripcion');
            $params['accion'] = 'crear';

            if ($this->valida($params)) // validamos los campos
                $result = $m->crearArticulo($params); // le decimos al Modelo que cree el artículo con los datos pasados
            else
                $result = null;
        }

        if ($result) { // si ha funcionado mostrará un mensaje
            $params['extra'] = '<div class="info inform"><h3>Artículo creado correctamente</h3></div>';
            require __DIR__ . '/templates/articulos.php';
        }
        else
            require __DIR__ . '/templates/crear_articulos.php';
    }

    public function modificar() {
        $m = new Model();
        $result = null;

        $articulo = $m->ArticuloRead($this->limpiaCampo('id')); // seleccionamos la ID del artículo a modificar

        $params = array(
            'id'=>$articulo->getId(),
            'nombre'=>$articulo->getNombre(),
            'precio'=>$articulo->getPrecio(),
            'descripcion'=>$articulo->getDescripcion(),
            'accion'=>'modificar'
        );

        if ($_SERVER['REQUEST_METHOD'] == 'POST') { // si hemos enviado el formulario, cogemos los datos de los campos
            $params['id'] = $this->limpiaCampo('id');
            $params['nombre'] = $this->limpiaCampo('nombre');
            $params['precio'] = $this->limpiaCampo('precio');
            $params['descripcion'] = $this->limpiaCampo('descripcion');
            $params['accion'] = 'modificar';

            if ($this->valida($params)) // validamos los campos
                $result = $m->modificarArticulo($params); // le decimos al Modelo que modifique el artículo con los datos pasados
            else
                $result = null;
        }

        if ($result) {
            $params['extra'] = '<div class="info inform"><h3>Artículo modificado correctamente</h3></div>';
            require __DIR__ . '/templates/articulos.php';
        }
        else
            require __DIR__ . '/templates/crear_articulos.php';
    }

    public function borrar() {
        $m = new Model();
        $result = $m->borrarArticulo($_REQUEST['id']); // borramos el artículo por la ID dada
        $params = array('articulos'=>$m->listaArticulos(true)); // volvemos a sacar la lista de los artículos actual

        require __DIR__ . '/templates/mostrar_articulos.php';
    }

    public function ver() {
        $m = new Model();

        $f = $m->ArticuloRead($this->limpiaCampo('id')); // elegimos el archivo del Modelo por la ID

        // pasamos el objeto por parámetros
        $params["articulos"][] = new Articulo($f->getId(), $f->getNombre(), $f->getPrecio(), $f->getDescripcion());

        require __DIR__ . '/templates/mostrar_articulos.php';
    }


    public function json() {
        $m = new Model();
        $articulos = $m->listaArticulos(false);
        $this->guardar_archivo("json", json_encode($articulos));
    }

    public function ver_json() {
        $m = new Model(); 
        $articulos = file_get_contents('../ficheros/articulos.json');
        $articulos = $this->arrayArticulos(json_decode($articulos, false)); // pasamos el JSON a array de Artículo
        $params = array('articulos'=> $articulos);
        $params["tipo"] = "JSON";

        require __DIR__ . '/templates/mostrar_articulos.php';
    }

    private function arrayArticulos($array) {
        $articulos = [];
        foreach ($array as $f) // convertimos el array asociativo en un array de objetos Artículo
        $articulos[] = new Articulo($f->id, $f->nombre, $f->precio, $f->descripcion);

        return $articulos; // devolvemos el array de objetos
    }

    public function csv() {
        $m = new Model();
        $articulos = $m->listaArticulos();

        $archivo = fopen('../ficheros/articulos.csv', "w");
        if ($archivo) {
            if (count($articulos) == 0) $articulos = []; // si el array está vacío daría error, así no muestra nada
            foreach($articulos as $linea) {
                // si es un objeto guarda con su método getCSV(), si no, es que es un array y guarda la línea entera
                if (is_object($linea))
                    $linea = explode(";",$linea->getCSV());
                fputcsv($archivo, $linea, ";"); }
            $params['extra'] = '<div class="info inform"><h3>Archivo CSV creado correctamente</h3></div>';
        } else
            $params['extra'] = '<div class="info error"><h3>Archivo CSV no se ha podido crear</h3></div>';

        require __DIR__ . '/templates/articulos.php';
    }

    public function ver_csv() {
        $array = [];
        
        $archivo = fopen('../ficheros/articulos.csv', "r"); // abrimos el archivo en modo lectura
        if ($archivo) {
            while (($f = fgetcsv($archivo, 1000,";")) !== false) // si no hay ningún error, añadimos la fila al array
                $array[] = (object) Array("id"=>$f[0],"nombre"=>$f[1],"precio"=>$f[2],"descripcion"=>$f[3]);
            fclose($archivo); // cerramos archivo
        }
        sort($array); // ordenamos el array antes de devolverlo, así su última Id siempre será la última posición
        $array = $this->arrayArticulos($array); // pasamos array CSV a array de Artículo
        $params = array('articulos'=>$array);
        $params["tipo"] = "CSV";

        require __DIR__ . '/templates/mostrar_articulos.php';
    }

    public function rss() {
        $rss = '';
        $rss .= "<rss version='2.0' xmlns:atom='http://www.w3.org/2005/Atom'>".PHP_EOL;
        $rss .= "<articulo>".PHP_EOL;

        $rss .= "<title>Artículos</title>".PHP_EOL;
        $rss .= "<description>Lista de Artículos</description>".PHP_EOL;
        $rss .= "<language>es-ES</language>".PHP_EOL;

        $m = new Model();
        $articulos = $m->listaArticulos(true);
    
        foreach ($articulos as $a) {
            $rss .= "<item>".PHP_EOL;
                $rss .= "<id>".$a->getId()."</id>".PHP_EOL;
                $rss .= "<nombre>".$a->getNombre()."</nombre>".PHP_EOL;
                $rss .= "<precio>".$a->getPrecio()."</precio>".PHP_EOL;
                $rss .= "<descripcion>".$a->getDescripcion()."</descripcion>".PHP_EOL;
            $rss .= "</item>".PHP_EOL;
        }

        $rss .= "</articulo>".PHP_EOL;
        $rss .= "</rss>".PHP_EOL;

        $this->guardar_archivo("rss", $rss);
    }

    public function ver_rss() {
        $rss = simplexml_load_string(file_get_contents('../ficheros/articulos.rss'));
        $articulos = [];
        foreach ($rss->articulo->item as $a)
            $articulos[] = $a;
        
        $params = array('articulos'=>$this->arrayArticulos($articulos));
        $params["tipo"] = "RSS";

        require __DIR__ . '/templates/mostrar_articulos.php';
    }

    private function valida($params) { // función para validar los datos introducidos en los campos de Artículo
        return is_string($params["nombre"]) & is_numeric($params["precio"]); // la ID es fija y la descripción opcional
    }

    private function ver_archivo($tipo) {
        $archivo = file_get_contents('../ficheros/articulos.'.$tipo);
        $params['extra'] = "<div class='code'><code><h3>Archivo ARTÍCULOS.".strtoupper($tipo).":</h3>$archivo</code></div>";

        require __DIR__ . '/templates/articulos.php';
    }

    private function guardar_archivo($tipo, $array) {
        $archivo = fopen('../ficheros/articulos.'.$tipo, 'w');
        $r = fwrite($archivo, $array);

        fclose($archivo);

        if ($r)
            $params['extra'] = '<div class="info inform"><h3>Archivo '.strtoupper($tipo).' creado correctamente</h3></div>';
        else
            $params['extra'] = '<div class="info error"><h3>Archivo '.strtoupper($tipo).' no se ha podido crear</h3></div>';

        require __DIR__ . '/templates/articulos.php';
    }

    private function limpiaCampo($campo) {
        $valor = false;
        if (isset($_REQUEST[$campo]))
            $valor = strip_tags(trim(htmlspecialchars($_REQUEST[$campo])));
        return $valor;
    }
}

?>