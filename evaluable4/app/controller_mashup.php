<?php

class ControllerMashup {

    public function mashup() {
        $url = 'http://mapas.valencia.es/lanzadera/opendata/Valenbisi/JSON';
        $estaciones = json_decode(file_get_contents($url)); // obenemos el archivo json de la url
        $estaciones = $estaciones->features; // limpiamos la cabecera y nos quedamos con el array que nos interesa
        $array = [];

        foreach($estaciones as $e) { // no queremos todos los atributos, así que preparamos un objeto nuevo
            $array[] = (object) Array(
                "id"=>$e->properties->number,
                "nombre"=>$e->properties->address,
                "disponibles"=>$e->properties->available,
                "libres"=>$e->properties->free,
                "total"=>$e->properties->total,
                "coordenadas"=>$e->geometry->coordinates
            );
        }

        sort($array); // lo ordenamos en orden ascendente, puesto que viene desordenado

        if ($_SERVER['REQUEST_METHOD'] == 'POST')
            $params["estacion"] = $array[$_POST["estacion"]-1]; // si hemos enviado el post, elegimos la id del select

        $params["estaciones"] = $array; // pasamos las estaciones como un parámetro

        require __DIR__ . '/templates/mashup.php';
    }
}

?>