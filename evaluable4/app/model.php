<?php

include_once('articulo.php');

class Model {

    protected $db;

    public function __construct()
    {
        $this->db = new PDO(
            'pgsql:host='.Config::$mvc_bd_hostname.
            ';dbname='.Config::$mvc_bd_nombre,
            // ';charset=utf8',
            Config::$mvc_bd_usuario,
            Config::$mvc_bd_clave);
    }

    // ARTÍCULOS

    private function consulta($consulta, $parametros) { // devuelve un array de columnas de la tabla seleccionada

        $result = $this->db->prepare($consulta); // preparamos la consulta
        // $r = $result->execute($parametros); // ejecutamos la consulta con parámetros
    
        if ($parametros) 
            $result->execute($parametros); // la ejecutamos, si tiene parámetros 
        else
            $result->execute(); // ...o sencilla, si no tiene parámetros

        // $this->db = false; // cerramos la conexión
       
        if ($result) // si la consulta ha devuelto algo...
            return $result->fetchAll(PDO::FETCH_OBJ); // devuelve un array de objetos
        else
            return null; // o no devuelve nada si la consulta está vacía
    }

    private function arrayArticulos($result) {
        $articulos = [];
        foreach ($result as $f) // convertimos el resultado en un array de objetos Artículo
        $articulos[] = new Articulo($f->id, $f->nombre, $f->precio, $f->descripcion);

        return $articulos; // devolvemos el array de objetos
    }

    public function listaArticulos($objeto = true) { // con true devolvemos un array de objetos Articulo, con false un array de la consulta
        $consulta = "SELECT * FROM articulo ORDER BY id ASC";
        $result = $this->consulta($consulta, null);
        
        return ($objeto) ? $this->arrayArticulos($result) : $result; // devolvemos el array de objetos
    }

    public function buscarArticulos($nombre) {
        $nombre = htmlspecialchars($nombre);
        $consulta = "SELECT * FROM articulo WHERE nombre LIKE ? ORDER BY id ASC";
        $parametros = array($nombre); // hacemos segura la consulta pasándole los parámetros
        $result = $this->consulta($consulta, $parametros);
        
        return $this->arrayArticulos($result); // devolvemos el array de objetos
    }

    public function ultimaID() {
        $consulta = "SELECT MAX(id) FROM articulo";
        $result = $this->consulta($consulta, null);

        return $result[0]->max+1;
    }

    function ArticuloRead($id){ // para leer el articulo con ese id. Devuelve el objeto del fichero
        $consulta = "SELECT * FROM articulo WHERE id=:id";
        $parametros = array(":id"=>$id); // definimos los parámetros, en este caso la ID

        $result = $this->consulta($consulta, $parametros);
        $f = $result[0];

        return (!$f) ? null : new Articulo($f->id, $f->nombre, $f->precio, $f->descripcion);
    }

    public function crearArticulo($param) {
        $consulta = "INSERT INTO articulo VALUES (:id, :nombre, :precio, :descripcion)";

        $parametros = array(
            ":id"=>$param["id"],
            ":nombre"=>$param["nombre"],
            ":precio"=>$param["precio"],
            ":descripcion"=>$param["descripcion"]
        );

        $result = $this->consulta($consulta, $parametros); // hacemos la consulta segura, pasándole los parámetros

        return $result;
    }

    function modificarArticulo($param){ // para actualizar el objeto articulo en un fichero
        $consulta = "UPDATE articulo SET nombre=:nombre, precio=:precio, descripcion=:descripcion WHERE id=:id";

        $parametros = array(
            ":id"=>$param["id"],
            ":nombre"=>$param["nombre"],
            ":precio"=>$param["precio"],
            ":descripcion"=>$param["descripcion"]
        );

        $result = $this->db->prepare($consulta); // preparamos la consulta
        $result->execute($parametros); // ejecutamos la consulta con parámetros
    
        return $result; // devuelve la consulta, ya sea correcta o no
    }

    function borrarArticulo($id){ // borramos un objeto de una tabla según una ID dada
        $consulta = "DELETE FROM articulo WHERE id=:id";
        $result = $this->db->prepare($consulta); // preparamos la consulta
        $parametros = array(":id"=>$id);
        $result->execute($parametros); // ejecutamos la consulta con parámetros

        return $result; // devuelve la consulta, ya sea correcta o no
    }

}

?>