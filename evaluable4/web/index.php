<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <link rel="stylesheet" type="text/css" href="css/estilos.css">
</head>

<body>
<div class="contenedor">
<?php
    require_once __DIR__ . '/../app/config.php';
    require_once __DIR__ . '/../app/model.php';
    require_once __DIR__ . '/../app/controller.php';

    $map = array(
        'inicio' => array('controller'=>'Controller','action'=>'inicio'),
        'mashup' => array('controller'=>'ControllerMashup','action'=>'mashup'),
        'salir' => array('controller'=>'Controller','action'=>'salir'),
        'articulos' => array('controller'=>'ControllerArticulos','action'=>'articulos'),
        'listar' => array('controller'=>'ControllerArticulos','action'=>'listar'),
        'buscar' => array('controller'=>'ControllerArticulos','action'=>'buscar'),
        'crear' => array('controller'=>'ControllerArticulos','action'=>'crear'),
        'ver' => array('controller'=>'ControllerArticulos','action'=>'ver'),
        'modificar' => array('controller'=>'ControllerArticulos','action'=>'modificar'),
        'borrar' => array('controller'=>'ControllerArticulos','action'=>'borrar'),
        'json' => array('controller'=>'ControllerArticulos','action'=>'json'),
        'ver_json' => array('controller'=>'ControllerArticulos','action'=>'ver_json'),
        'csv' => array('controller'=>'ControllerArticulos','action'=>'csv'),
        'ver_csv' => array('controller'=>'ControllerArticulos','action'=>'ver_csv'),
        'rss' => array('controller'=>'ControllerArticulos','action'=>'rss'),
        'ver_rss' => array('controller'=>'ControllerArticulos','action'=>'ver_rss')
    );

    if (isset($_GET['ctl'])) {
        if (isset($map[$_GET['ctl']]))
            $ruta = $_GET['ctl'];
        else {
            header('Status: 404 Not Found');
            echo '<div class="info error"><h3>Error 404: No existe la ruta <i>'.$_GET['ctl'].'</i></h3></div>';
            exit;
        }

    } else
        $ruta = 'inicio';

    $controlador = $map[$ruta];

    if (method_exists($controlador['controller'],$controlador['action']))
        call_user_func(array(new $controlador['controller'],$controlador['action']));
    else {
        header('Status: 404 Not Found');
        echo '<div class="info error"><h3>Error 404: No existe el controlador<i>'.$controlador['controller'].'->'.$controlador['action'].'</i></h3></div>';
    }
?>
</div>
</body></html>