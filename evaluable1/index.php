<!DOCTYPE html>
<html>

<head>
    <link rel="stylesheet" type="text/css" href="estilos.css">
    <meta charset="utf-8">
</head>
<?php
    include("datos.php");
    include("includes.php");

    $usuario=$datos["usuario"];
    if (!$usuario)
        header("location:login.php");
    else {
    
    $arry["usuario"] = $usuario;
    $arry["clientes"] = $clientes;
    $arry["articulos"] = $articulos;
    $arry["pedidos"] = $pedidos;
    $arry["lineas"] = $lineas;

    $datos=array_envia($arry);
    }

?>
<body>
    <div>
        <h1>Inicio</h1>
        <h2>Menú</h2>
    <ul class="menu">
        <li><a href="articulos.php<?php echo "?datos=$datos"?>">Artículos</a></li>
        <li><a href="clientes.php<?php echo "?datos=$datos"?>">Clientes</a></li>
        <li><a href="pedidos.php<?php echo "?datos=$datos"?>">Pedidos</a></li>
    </ul>
    <h2>Información</h2>
    <ul class="menu">
        <li><a href="enunciado.html">Enunciado de la práctica</a></li>
        <li><a href="documentacion.html">Documentación del alumno</a></li>
        <li><a href="https://bitbucket.org/angeldefez/angeldefezdws">BitBucket alumno</a></li>
        <li><a href="https://angeldefezdws.herokuapp.com">Heroku alumno</a></li>
    </ul>
    <a class="derecha" href="../index.html">Volver</a>
    <footer><p>Evaluable 1 DWES | Ángel de Fez | 31-10-2019</p></footer>
</div>
</body>
</html>