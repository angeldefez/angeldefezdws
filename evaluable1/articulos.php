<!DOCTYPE html>
<html>

<head>
    <link rel="stylesheet" type="text/css" href="estilos.css">
    <meta charset="utf-8">
</head>

<body>
    <div>
    <h1>Artículos</h1>
    <h2>Menú</h2>
    <?php
    include("includes.php");
    $tabla = "articulos";
    $campos = array("ID","Nombre","Precio","Descripcion"); // los campos que contiene tanto el formulario como la tabla
    
    // interceptamos la opción elegida y mostramos un menú u otro, según convenga
    if ($op=="e") { // resultado de un formulario
        $array = validarDatos($tabla, $campos);
        procesaResultado($tabla, $array);
    }
    else
        mostrarMenu($tabla);
   
    ?>
    <span class="limpia"></span>
    <footer><p>Evaluable 1 DWES | Ángel de Fez | 31-10-2019</p></footer>
    </div>
    <?php
    // muestra el formulario, cada tabla tiene el suyo propio
        function mostrarFormulario($array, $id, $op) {
            $nombre = isset($array["Nombre"]) ? $array["Nombre"] : null;
            $precio = isset($array["Precio"]) ? $array["Precio"] : null;
            $descripcion = isset($array["Descripcion"]) ? $array["Descripcion"] : null;
            global $envia;
            $titulo = ($op == 'c') ? "Crear Nuevo":"Modificar";
        
            echo '<form name="formulario" method="post" action="articulos.php?op=e&id='.$id.'&datos='.$envia.'">';
            echo '<fieldset>';
            echo '<legend>'.$titulo.' Artículo</legend>';
            echo '<p>ID: <input type="text" name="ID" size="5" required readonly value='.$id.'></p>'; // sólo lectura, no se puede modificar
            echo '<p>Nombre: <input type="text" name="Nombre" size="80" required value="'.$nombre.'"></p>';
            echo '<p>Precio: <input type="text" name="Precio" size="6" required value="'.$precio.'"></p>';
            echo 'Descripción: <p><textarea name="Descripcion" rows="5" cols="50">'.$descripcion.'</textarea></p>'; // no sé por qué pero se bloquea en Firefox
            echo '<input name="o" type="hidden" value="'.$op.'">';
            echo '<input type="submit" value="Enviar">';
            echo '<input type="reset" value="Reiniciar">';
            echo '</fieldset>';
            echo '</form>';
        }
?>
</html>