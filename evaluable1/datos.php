<?php

if(!isset($datos)) { // si noy hay datos guardados en el array $datos es porque es la primera vez que entramos, los añadimos
// arrays convertidos desde el CVS gracias a: https://shancarter.github.io/mr-data-converter/
$clientes = array(
    array("ID"=>1,"Nombre"=>"Ángel de Fez","Email"=>"angeldefez@email.com","Usuario"=>"angeldefez","Password"=>1234,"DNI"=>"12345678X","Foto"=>"angeldefez.jpg"),
    array("ID"=>2,"Nombre"=>"Paco Aldarias","Email"=>"pacoaldarias@ceedcv.com","Usuario"=>"pacoaldarias","Password"=>1234,"DNI"=>"12345678A","Foto"=>"pacoaldarias.png"),
    array("ID"=>3,"Nombre"=>"Joaquín Gilabert","Email"=>"joaquingilabert@ceedcv.com","Usuario"=>"joaquingilabert","Password"=>1234,"DNI"=>"12345679B","Foto"=>"joaquin.jpg"),
    array("ID"=>4,"Nombre"=>"Sandra Palmo","Email"=>"sandrapalmo@ceedcv.com","Usuario"=>"sandrapalmo","Password"=>1234,"DNI"=>"12345680C","Foto"=>"sandra.jpg"),
    array("ID"=>7,"Nombre"=>"Pablo Martín","Email"=>"pablomartin@ceedcv.com","Usuario"=>"pablomartin","Password"=>1234,"DNI"=>"12345682E","Foto"=>"pablo.jpg"),
    array("ID"=>5,"Nombre"=>"Irene Espinar","Email"=>"irene3spinar@ceedcv.com","Usuario"=>"irene3spinar","Password"=>1234,"DNI"=>"12345681D","Foto"=>"irene.jpg")
  );

 $lineas = array(
    array("ID"=>1,"Cantidad"=>1,"Fecha"=>"21/10/2019","idPedido"=>1,"idCliente"=>1,"idProducto"=>10),
    array("ID"=>2,"Cantidad"=>1,"Fecha"=>"21/10/2019","idPedido"=>1,"idCliente"=>1,"idProducto"=>8),
    array("ID"=>3,"Cantidad"=>1,"Fecha"=>"21/10/2019","idPedido"=>1,"idCliente"=>1,"idProducto"=>5),
    array("ID"=>4,"Cantidad"=>2,"Fecha"=>"21/10/2019","idPedido"=>2,"idCliente"=>3,"idProducto"=>1)
  );

$pedidos = array(
    array("ID"=>1,"Fecha"=>"22/10/2019","idCliente"=>1,"Notas"=>"Preferiblemente por la mañana."),
    array("ID"=>2,"Fecha"=>"23/10/2019","idCliente"=>3,"Notas"=>"Dejar el paquete en la portería.")
  );

$articulos = array(
    array("ID"=>1,"Nombre"=>"Kingston A400 SSD 240GB","Precio"=>32.99,"Descripcion"=>"Se trata del modelo más modesto de los que Kingston tiene actualmente en el mercado y se dirige a usuarios de presupuesto limitado que busquen sustituir su disco duro mecánico para incrementar el rendimiento de su equipo."),
    array("ID"=>2,"Nombre"=>"Xiaomi Mi Air 13.3","Precio"=>899,"Descripcion"=>"El Xiaomi Mi Notebook Aires ligero, es compacto, y podrás llevarlo fácilmente a cualquier lugar sin darte cuenta de que lo llevas encima."),
    array("ID"=>3,"Nombre"=>"Huawei P20 Lite 64GB Azul Libre","Precio"=>233.24,"Descripcion"=>"Huawei P20 Lite parece estar destinado a ser un superventas, con una relación calidad/precio sorprendente."),
    array("ID"=>4,"Nombre"=>"LG 43UK6200PLA 43\" LED IPS UltraHD 4K","Precio"=>299,"Descripcion"=>"La mejor pantalla calidad precio de toda la página, es 4K nativo."),
    array("ID"=>5,"Nombre"=>"Logitech B100 Ratón Negro","Precio"=>4.99,"Descripcion"=>"Cómodo, diseño ambidiestro La forma cómoda y de bajo perfil se siente bien en cualquier mano, incluso después de un largo día de trabajo."),
    array("ID"=>7,"Nombre"=>"Samsung UE50NU7025 50\" LED UltraHD 4K","Precio"=>385,"Descripcion"=>"Vive una experiencia visual a todo color. Por fin podrás disfrutar de tu contenido preferido con colores más vivos y realistas."),
    array("ID"=>8,"Nombre"=>"Kobo Clara HD eReader 6\" ","Precio"=>129,"Descripcion"=>"El Kobo Clara HD es el compañero perfecto para cualquier amante de la lectura. Siempre proporciona la mejor luz de lectura, gracias a la ComfortLight PRO, y es garantía de una experiencia de lectura natural, como si se tratase de papel impreso, con su excepcional pantalla HD de 6."),
    array("ID"=>9,"Nombre"=>"Canon Pixma MX475 Multifunción WiFi/Fax","Precio"=>52.35,"Descripcion"=>"Equipo Todo en Uno para oficina doméstica con conectividad inalámbrica y alimentador automático de documentos Imprime, copia, escanea y envía fax con facilidad con este asequible equipo Todo en Uno Wi-Fi."),
    array("ID"=>10,"Nombre"=>"Grand Theft Auto V PS4","Precio"=>28.63,"Descripcion"=>"Vive la historia de Trevor, Michael y Franklin a través de Los Santos y el condado de Blaine. Juega la campaña principal o piérdete entre sus calles con las innumerables tareas y actividades opcionales a las que podrás acceder.")
  );
}
