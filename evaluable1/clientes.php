<!DOCTYPE html>
<html>

<head>
    <link rel="stylesheet" type="text/css" href="estilos.css">
    <meta charset="utf-8">
</head>

<body>
    <div>
    <h1>Clientes</h1>
    <h2>Menú</h2>
    <?php
    include("includes.php");
    $tabla = "clientes";
    $campos = array("ID","Nombre","Email","Usuario","DNI"); // los campos que contiene tanto el formulario como la tabla

    // interceptamos la opción elegida y mostramos un menú u otro, según convenga
    if ($op == "r") // read
        echo '<img class="foto" src="imagenes/'.devuelveCampo($tabla, $id, "Foto").'" alt="Foto de cliente" height="100" width="100">';
    if ($op=="e") { // resultado de un formulario
        $array = validarDatos($tabla, $campos);
        // hay campos que es mejor validarlos a mano
        $array["Password"] = (limpiaCampo("password1") == limpiaCampo("password2") ? limpiaCampo("password1") : null);
        $array["Foto"] = obtenerFoto("Foto","imagenes/");
        procesaResultado($tabla, $array);
    }
    else
        mostrarMenu($tabla);
    ?>
    <span class="limpia"></span>
    <footer><p>Evaluable 1 DWES | Ángel de Fez | 31-10-2019</p></footer>
    </div>

    <?php
        // muestra el formulario, cada tabla tiene el suyo propio
        function mostrarFormulario($array, $id, $op) {
            $nombre = isset($array["Nombre"]) ? $array["Nombre"] : null;
            $email = isset($array["Email"]) ? $array["Email"] : null;
            $password = isset($array["Password"]) ? $array["Password"] : null;
            $usuario = isset($array["Usuario"]) ? $array["Usuario"] : null;
            $dni = isset($array["DNI"]) ? $array["DNI"] : null;
            $foto = isset($array["Foto"]) ? $array["Foto"] : null;
            global $envia;
            $titulo = ($op == 'c') ? "Crear Nuevo":"Modificar";
        
            echo '<form name="formulario" method="post" action="clientes.php?op=e&id='.$id.'&datos='.$envia.'" enctype="multipart/form-data">';
            echo '<fieldset>';
            echo '<legend>'.$titulo.' Cliente</legend>';
            echo '<img class="form" src="imagenes/'.$foto.'" alt="Foto de cliente" height="100" width="100">';
            echo '<p>ID: <input type="text" name="ID" size="5" required readonly value='.$id.'></p>'; // sólo lectura, no se puede modificar
            echo '<p>Nombre y apellidos: <input type="text" name="Nombre" size="50" required value="'.$nombre.'"></p>';
            echo '<p>E-mail: <input type="text" name="Email" size="30" required value="'.$email.'"></p>';
            echo '<p>Nombre de usuario: <input type="text" name="Usuario" size="20" required value="'.$usuario.'"></p>';
            echo '<p>DNI: <input type="text" name="DNI" size="20" required value="'.$dni.'"></p>';
            echo '<p>Contraseña: <input type="password" name="password1" size="20" required value="'.$password.'"> Introduce de 7 a 20 caráceres</p>';
            echo '<p>Repite contraseña: <input type="password" name="password2" size="20" required value="'.$password.'"></p>';
            echo '<p>Foto: <input type="file" name="Foto" value="'.$foto.'"/></p>';
            echo '<input type="hidden" name="max_file_size" value="102400" >';
            echo '<input name="o" type="hidden" value="'.$op.'">';
            echo '<input type="submit" value="Enviar">';
            echo '<input type="reset" value="Reiniciar">';
            echo '</fieldset>';
            echo '</form>';
        }

        // función muy útil para añadir o modificar elementos dentro de la tabla
        function validarDatosClientes($tabla, $campos) {
            $array = []; // almacenaremos un array para enviarlo y añadirlo a la tabla correspondiente
            foreach ($campos as $i) // recorremos todos los campos y añadimos al array el valor pertinente
                $array[$i] = limpiaCampo($i); // hacemos un REQUEST de cada campo, limpiando el texto previamente
            // hay campos que hay que validar a mano
            $array["Password"] = (limpiaCampo("password1") == limpiaCampo("password2") ? limpiaCampo("password1") : null);
            $array["Foto"] = obtenerFoto("Foto","imagenes/");

            procesaResultado($tabla, $array); // procesa el resultado y hace las operaciones pertinentes
        }
    ?>
</body>
</html>