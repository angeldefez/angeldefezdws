<?php
include("vector.php");
include("formularios.php");

// variables globales
// $a_clientes = "./datos/clientes.csv";
// $clientes = CSVaArray($a_clientes);

$op=(isset($_GET["op"])) ? $_GET["op"] : null; // obtenemos el código de la operación de la url, de haberlo
$id=(isset($_GET["id"])) ? $_GET["id"] : null; // obtenemos el código id de la url, de haberlo
$url_anterior = (isset($_SERVER['HTTP_REFERER'])) ? $_SERVER['HTTP_REFERER'] : null;
$url_actual = $_SERVER['PHP_SELF'];

$botones_urls=array("?op=r","?op=u","?op=d"); // las opciones de los botonos de la tabla
$botones_texto=array("Ver","Modificar","Borrar"); // la etiqueta de cada botón de la tabla

// preparamos los datos para mandarlos en cada url
$datos=(isset($_GET["datos"])) ? $_GET["datos"] : null;
if ($datos) {
    $datos = array_recibe($datos);
    $clientes = $datos["clientes"];
    $articulos = $datos["articulos"];
    $pedidos = $datos["pedidos"];
    $lineas = $datos["lineas"];
    $usuario = $datos["usuario"];

    $envia = array_envia($datos);
}
