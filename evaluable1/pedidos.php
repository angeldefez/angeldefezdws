<!DOCTYPE html>
<html>

<head>
    <link rel="stylesheet" type="text/css" href="estilos.css">
    <meta charset="utf-8">
</head>

<body>
    <div>
    <h1>Pedidos</h1>
    <h2>Menú</h2>
    <?php
    include("includes.php");
    $tabla = "pedidos";
    $campos = array("ID","Fecha","idCliente","Notas"); // los campos que contiene tanto el formulario como la tabla
    
    // interceptamos la opción elegida y mostramos un menú u otro, según convenga
    if ($op=="e") { // resultado de un formulario
        $array = validarDatos($tabla, $campos);
        procesaResultado($tabla, $array);
    }
    else
        mostrarMenu($tabla);
   
    ?>
    <span class="limpia"></span>
    <footer><p>Evaluable 1 DWES | Ángel de Fez | 31-10-2019</p></footer>
    </div>
    <?php
    // muestra el formulario, cada tabla tiene el suyo propio
        function mostrarFormulario($array, $id, $op) {
            $fecha = isset($array["Fecha"]) ? $array["Fecha"] : null;
            $idCliente = isset($array["idCliente"]) ? $array["idCliente"] : null;
            $notas = isset($array["Notas"]) ? $array["Notas"] : null;

            global $envia;
            $titulo = ($op == 'c') ? "Crear Nuevo":"Modificar";
        
            echo '<form name="formulario" method="post" action="pedidos.php?op=e&id='.$id.'&datos='.$envia.'">';
            echo '<fieldset>';
            echo '<legend>'.$titulo.' Artículo</legend>';
            echo '<p>ID: <input type="text" name="ID" size="5" required readonly value='.$id.'></p>'; // sólo lectura, no se puede modificar
            echo '<p>Fecha: <input type="text" name="Fecha" size="10" required value="'.$fecha.'"></p>';
            echo '<p>ID cliente: <input type="text" name="idCliente" size="6" required value="'.$idCliente.'"></p>';
            echo 'Notas: <br><textarea name="Notas" rows="5" cols="80">'.$notas.'</textarea><br>'; // no sé por qué pero se bloquea en Firefox
            echo '<input type="submit" value="Enviar">';
            echo '<input type="reset" value="Reiniciar">';
            echo '<input name="o" type="hidden" value="'.$op.'">';
            echo '</fieldset>';
            echo '</form>';
        }
    ?>

</body>
</html>