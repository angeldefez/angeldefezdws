<?php

// función de desinfección de código, compartida por el profesor
function limpiaCampo($campo) {
    $valor = false;
    if (isset($_REQUEST[$campo]))
        $valor = strip_tags(trim(htmlspecialchars($_REQUEST[$campo])));
    return $valor;
}

function generaPassword($n) {
    $car = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ'.'abcdefghijklmnopqrstuvwxyz'.'0123456789'.'!_"@$-~&.*';
    $pass = "";
    for ($i=0; $i<$n; $i++)
        $pass .= $car[rand(0,strlen($car)-1)];
    return $pass;
}

function crearEnlace($url, $texto, $clase = false) {
    echo "<a ".(($clase) ? "class='$clase'" : "")."href='$url'>$texto</a>";
}

// muestra un menú diferente según la opción elegida
function mostrarMenu($nombre) {
    global $op, $url_actual, $id, $envia, $datos;
    $tabla = $datos[$nombre];
    switch ($op) {
        case "c":
            $nuevaID = ultimaID($tabla)+1;
            mostrarFormulario(devuelveArrayPorID($tabla,$nuevaID),$nuevaID, $op);
            crearEnlace($url_actual."?datos=$envia","Volver","derecha");
            break;
        case "r":
            crearEnlace($url_actual."?op=c&datos=$envia","Crear nuevo","derecha");
            arrayAtabla(devuelveArrayPorID($tabla,$id));
            crearEnlace($url_actual."?datos=$envia","Volver","derecha");
            break;
        case "u":
            mostrarFormulario(devuelveArrayPorID($tabla,$id),$id, $op);
            crearEnlace($url_actual."?datos=$envia","Volver","derecha");
            break;
        case "d": 
            borrarPorID($tabla,$id);
            $datos[$nombre] = $tabla; // actualizamos los datos
            $envia = array_envia($datos);
            crearEnlace($url_actual."?op=c&datos=$envia","Crear nuevo","derecha");
            arrayAtablaBotones($tabla,"&datos=$envia");
            break;
        default:
            crearEnlace($url_actual."?op=c&datos=$envia","Crear nuevo","derecha");
            arrayAtablaBotones($tabla,"&datos=$envia");
    }
    crearEnlace("index.php?datos=$envia","Inicio","derecha");
}

// valida los datos de un array según su nombre de clave
function validarDatosArray($array) {
    $error = []; // para mostrar los posibles errores

    foreach ($array as $clave => $valor) {
        switch ($clave) {
            case "Email":
                if (!validaEmail($valor))
                    $error[] = $clave." incorrecto";
                break;
            case "DNI":
                if (!validaDNI($valor))
                    $error[] = $clave." incorrecto. Introduzca 7 u 8 dígitos y una letra. Puede ir separada por un guión.";
                break;
            case "Password":
                if (!validaPassword($valor))
                    $error[] = $clave." incorrecto. Solo se permiten de 7 a 20 letras, números y los siguientes 
                    caracteres: !_\"@$~&\.*";
                break;
            case "Fecha":
                if (!validaFecha($valor))
                    $error[] = $clave." incorrecta. Se permiten dd/mm/aaaa dd-mm-aaaa y dd.mm.aaaa";
                break;
            case "Cantidad":
            case "ID":
            case "idCliente":
                if (!validaEntero($valor))
                    $error[] = $clave." incorrecto. No es un número entero";
                break;
            default:
        }
    }

    return $error;
}

// función para procesar el resultado del formulario
function procesaResultado($tabla, $array) {
    $error = validarDatosArray($array);
    global $envia, $op, $id;
    $op = limpiaCampo("o");
    if (count($error) > 0) { // si error tiene algún dato es que ha fallado algo en la validación, lo mostramos
        echo "<h3>Error al validar el formulario</h3>";
        imprimirArray($error);
        crearEnlace($tabla.".php?op=$op&id=$id&datos=$envia","Volver","derecha");
    } else {
        echo "<h3>Datos correctos</h3>";
        echo "<p>Se ha actualizado la base de datos correctamente.</p>";
        if ($op == "u") {
            actualizarTabla($tabla, $id, $array);
        } else {
            crearNuevo($tabla, $array);
        }
        crearEnlace("$tabla.php"."?datos=$envia","Volver","derecha"); 
    }
    crearEnlace("index.php?datos=$envia","Inicio","derecha");
}

// función muy útil para añadir o modificar elementos dentro de la tabla
function validarDatos($tabla, $campos) {
    $array = []; // almacenaremos un array para enviarlo y añadirlo a la tabla correspondiente
    foreach ($campos as $i) // recorremos todos los campos y añadimos al array el valor pertinente
        $array[$i] = limpiaCampo($i); // hacemos un REQUEST de cada campo, limpiando el texto previamente
    return $array;
}

function obtenerFoto($campo, $target_path) {
    $target_path = $target_path . basename($_FILES[$campo]['name']);
    if (move_uploaded_file($_FILES[$campo]['tmp_name'], $target_path))
        return basename($_FILES[$campo]['name']);
    else
        return null;
}

// validadores
function validaEmail($texto) {
    $reg = "/^\w+@\w+\..{2,3}$/";
    return (preg_match($reg, $texto)) ? $texto : false;
}

function validaDNI($texto) {
    $reg = "/^\d{7,8}-?[a-z]$/i";
    return (preg_match($reg, $texto)) ? $texto : false;
}

function validaPassword($texto) {
    $reg = "/^[0-9a-z!_\"@$\-~&.*]{7,20}$/i";
    return (preg_match($reg, $texto)) ? $texto : false;
}
function validaFecha($texto) {
    $reg = "/^([0-3])?[0-9]{1}(\/|\-|\.){1}([0-2])?[0-9]{1}(\/|\-|\.){1}[0-9]{2,4}$/";
    return (preg_match($reg, $texto)) ? $texto : false;
}

function validaEntero($texto) {
    $reg = "/^\d+$/";
    return (preg_match($reg, $texto)) ? $texto : false;
}