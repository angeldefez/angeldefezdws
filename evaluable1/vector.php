<?php

// muestra una tabla según el contenido del array, con cada clave como encabezado de la misma
function arrayAtabla($array) {
    $multi = isset($array[0]); // comprueba si el array es multidimensional o no
    echo "<table><tr>";
    $cabecera = ($multi) ? $array[0] : $array; //si es un array multidimensional necesitamos elegir 1 elemento
    foreach($cabecera as $clave => $valor){
        echo "<th>$clave</th>";
    }
    echo "</tr>";
    foreach($array as $clave) {
        if ($multi) { // si es un array múltiple, hay que mostrarlo junto a <tr></tr>
            echo "<tr>";
            foreach($clave as $valor){
                echo "<td>{$valor}</td>";
            }
            echo "</tr>";
        } else { // si no es un array múltiple, mostramos su único valor
            echo "<td>{$clave}</td>";
        }
    }
    echo "</table>";
}

// añade botones de acciones en cada campo, para luego visualizarlo en la tabla
function arrayAtablaBotones($array, $texto = null) {
    $nuevo = Array();
    foreach($array as $campo) {
        $link="";
        global $botones_urls, $botones_texto, $url_actual; // usamos variables globales
        for ($i=0; $i<count($botones_urls); $i++) {
            $url = $url_actual.$botones_urls[$i]."&id=".$campo["ID"]."$texto";
            $link .= "<a href='$url'>$botones_texto[$i]</a>";
        }
        $campo["Acciones"] = $link;
        $nuevo[] = $campo;
    }
    arrayAtabla($nuevo);
}

// actualiza una tabla según su ID con un campo dado
function actualizarTabla($tabla, $id, $campos) {
    global $datos, $envia;
    $array = $datos[$tabla];
    global $clientes;
    $pos=buscarPorID($clientes, $id);
    foreach($campos as $clave => $valor) {
        $array[$pos][$clave] = $valor;}
    $datos[$tabla][$pos]=$array[$pos];
    $envia = array_envia($datos);
}

// añade nuevos datos al final de un array
function crearNuevo($tabla, $array) {
    global $datos, $envia;
    array_push($datos[$tabla], $array);
    $envia = array_envia($datos);
}

// devuelve la última ID de un conjunto, perfecto para añadir más elementos
function ultimaID($array) {
    if (count($array) > 0) { // si el array no está vacío
        sort($array); // lo ordenamos
        return ($array[count($array)-1]["ID"]); // devolvemos la ID de la última posición
    } else
    return 0; // si está vacío, devolvemos ID 0 (cuidado con colisión de datos al borrar)
}

// borra un array según la ID dada
function borrarPorID(&$array, $id) {
    $pos = buscarPorID($array, $id);
    unset($array[$pos]);
    sort($array); // si no lo re-ordenamos, da error a la hora de mostrar las tablas
}

// devuelve la posición de un array con la ID dada
function buscarPorID($array, $id) {
    $i = 0;
    foreach($array as $clave) {
        if ($clave["ID"] == $id)
            return $i; // devolvemos la posición dónde hemos encontrado dicha ID
        $i++;
    }
    return null;
}

// devuelve un array según la ID dada
function devuelveArrayPorID($array, $id) {
    $i = 0;
    foreach($array as $clave) {
        if ($clave["ID"] == $id)
            return $array[$i]; // devolvemos el array entero para trabajar con él
        $i++;
    }
    return null;
}

// agrega campos al final de un array dado
function agregarCampos(&$array, $campos) {
    $array[] = $campos;
}

// serializar los arrays para enviarlos entre páginas
function array_envia($array) {
    $tmp = serialize($array);
    $tmp = urlencode($tmp);
    return $tmp;
}

// unserializa el array enviado de otra página
function array_recibe($url_array) {
    $tmp = stripslashes($url_array);
    $tmp = urldecode($tmp);
    $tmp = unserialize($tmp);
   return $tmp;
}

// imprime todos los elementos de un array separados por saltos <br>
function imprimirArray($array) {
    foreach ($array as $i)
        echo $i."<br>";
}

// devuelve un campo según una ID dada, por ejemplo el valor de Nombre
function devuelveCampo($tabla, $id, $campo) {
    global $datos;
    $tabla = $datos[$tabla];
    $array = devuelveArrayPorID($tabla, $id);
    return $array[$campo];
}

// devuelve un array según el valor de uno de sus campos, como por ejemplo idCliente
function devuelvePorValor($tabla, $campo, $valor) {
    $i=0;
    $arry = [];
    foreach ($tabla as $array) {
        if ($array[$campo]==$valor)
            return $array;
        $i++;
    }
    return null;
}

/* NO LA UTILIZARÉ DE MOMENTO PUESTO QUE NO ESTÁ PERMITIDO EN ESTA EVALUABLE
function CSVaArray($n_archivo) {
    $array = array();
    $cabecera = array();
    $i = 0;
    
    $archivo = fopen($n_archivo, "r");
    if ($archivo) {
        while (($fila = fgetcsv($archivo, 1000)) !== false) {
            if (empty($cabecera)) {
                $cabecera = $fila;
                continue;
            }
            foreach ($fila as $c => $v) {
                $array[$i][$cabecera[$c]] = $v;
            }
            $i++;
        }
        if (!feof($archivo)) {
            echo "No se puede leer archivo\n";
        }
        fclose($archivo);
    }
    sort($array); // ordenamos el array antes de devolverlo, así su última Id siempre será la última posición
    return $array;
}
*/